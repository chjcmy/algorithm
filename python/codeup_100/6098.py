if __name__ == '__main__':
    h, w = map(int, input().split())
    # 맵을 만든다
    arr = [[0] * w for _ in range(h)]
    #테스트 카운트를 받는다
    tc = int(input())

    #테스트 카운트 만큼 반복
    for i in range(tc):
        #길이, 방향, x좌표, y좌표
        l, d, y, x = map(int, input().split())
        y -= 1
        x -= 1
        # 가로 경우
        if d == 0:
            for j in range(l):
                arr[y][x+j] = 1
        else:
            for j in range(l):
                arr[y+j][x] = 1
    #맵을 출력한다
    for i in range(h):
        for j in range(w):
            print(arr[i][j], end=" ")
        print()