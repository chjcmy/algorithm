if __name__ == '__main__':
    a, b, c = map(int, input().split())
    d = float(a) + float(b) + float(c)
    print(a+b+c,"%0.2f" % round(d/3, 2))