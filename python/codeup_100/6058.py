if __name__ == '__main__':
    a, b = map(int, input().split())
    if not bool(a) and not bool(b):
        print(True)
    else:
        print(False)
