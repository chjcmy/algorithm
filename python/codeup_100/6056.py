if __name__ == '__main__':
    a, b = map(int, input().split())
    c, d = bool(a), bool(b)
    print((c & (not d)) or ((not c) & d))