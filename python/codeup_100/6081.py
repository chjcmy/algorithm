if __name__ == '__main__':
    n = int(input(), 16)  # 16진법으로 바꾸기

    for i in range(1, 16):
        print('%X' % n, '*%X' % i, '=%X' % (n * i), sep='')
