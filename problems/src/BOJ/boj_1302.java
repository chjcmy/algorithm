package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/*
    제일 많이 책을 찾아라, 하지만 권수가 같을 경우 사전 순으로 가장 앞서는 제목으로 출력한다

    1. T만큼 배열을 만든다
    2. 배열 만큼 값을 받는다.
    3. 배열을 재 정렬 한다.
    4. 해쉬 맵을 만들어서 책이름이 없을 경우에는 값을 추가 하고 해쉬 맵에 책이름이 있을경우 책 이름 값에 1을 추가 시킨다
    5. max 변수를 만들고 제일 큰 값을 받는다
    6. 맵을 정렬 시킨뒤 max와 같은 값이 나왔을 경우 바로 책 이름을 출력하고 
 */
public class boj_1302 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1302.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        HashMap<String, Integer> bookMap = new HashMap<>();

        String str;

        for (int i = 0; i < T; i++) {
            str = br.readLine();
            if (bookMap.containsKey(str)) {
                bookMap.replace(str, bookMap.get(str) + 1);
            } else {
                bookMap.put(str, 1);
            }
        }

        int max = 0;

        for (String bookName : bookMap.keySet()) {
            max = Math.max(max, bookMap.get(bookName));
        }

        ArrayList<String> al = new ArrayList<>(bookMap.keySet());

        Collections.sort(al);

        for (String a : al) {
            if (bookMap.get(a) == max) {
                System.out.println(a);
                break;
            }
        }
    }
}
