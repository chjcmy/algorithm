package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 재용이는 최신 컴퓨터 "10대"를 가지고있다. 어느 날 재용이는 많은 데이터를 처리해야될일이 생겨서 각 컴퓨터에 1 번부터 10번까지의 번호를 부여하고,
 * 10대의 컴퓨터가 다음 과 같은 방법 으로 데이터를 처리하기로 하였다. 1번 데이터는 1번 컴퓨터, 2번 데이터는 2번 컴퓨터, 3번 데이터는 3번 컴퓨터, ...,
 * 10번 데이터는 10번 컴퓨터, 11번 데이터는 1 번 컴퓨터, 12번 데이터는 2번 컴퓨터, ...
 * 총 데이터의 개수는 항상 a b 개 의 형태로 주어진다. 재용이는 문득 마지막 데이터가 처리될 컴퓨터 번호가 궁금 해졌다. 이를 구하라
 *
 * 1. T를 받는다
 * 2. T에 대해 일차원 배열을 두개 만든다.
 * 3. 테스트 개수에 맞게 a, b값을 받아 a, b 배열에 넣는다
 * 4. 테스트 개수에 맞게 반복 문을 만든다
 * 5. i에 맞게 a[i]를 b[i]에 맞게 연속적으로 곱해준다.
 * 6. 갑이 나왔을시 10 으로 나누어서 나머지를 출력해준다
 */

public class boj_1009 {

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1009.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder();

        int T = Integer.parseInt(br.readLine());

        StringTokenizer st;

        int[] aArr = new int[T];
        int[] bArr = new int[T];


        for (int i = 0; i < T; i++) {
            st = new StringTokenizer(br.readLine());

            aArr[i] = Integer.parseInt(st.nextToken());
            bArr[i] = Integer.parseInt(st.nextToken());
        }

        for (int i = 0; i < T; i++) {
            int result = 1;
            for (int j = 0; j < bArr[i]; j++) {
                result=(result*aArr[i])%10;
            }
            if(result==0) {
                result=10;
            }

            sb.append(result).append('\n');
        }
        System.out.println(sb);
        br.close();
    }
}
