package BOJ;

/*
    방향 없는 그래프가 주어졌을때 연결 요소의 개수를 구하여라
    % 연결 요소
    * 모든 정점을 연결하는 경로가 있어야 한다.
    * 또 다른 연결 요소에 속한 정점과 연결 하는 경로가 있으면 안된다

    1. 노드 개수를 n에 저장하고 에지의 개수를 받는다
    2. 연결할 부분에 연결할 부분을 추가 한다.
    3. true false 배열을 만든다
    4. 1 부터 시작 하면서 dfs 가 돌면서 true false 배열을 채운다.
    5. 만약 노드가 끝났을 경우 포 문이 다음으로 넘어 갈때는 cnt++ 을 해준다
    6. true false 배열이 true 로 채워 졌을 시  cnt 를 출력 한다.

 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;


public class boj_11724 {

    static ArrayList<Integer>[] A;
    static boolean[] visited;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/11724.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int N = Integer.parseInt(st.nextToken());
        int M = Integer.parseInt(st.nextToken());

        A = new ArrayList[N + 1];
        visited = new boolean[N + 1];

        for (int i = 1; i < N + 1; i++) {
            A[i] = new ArrayList<Integer>();
        }

        for (int i = 0; i < M; i++) {
            st = new StringTokenizer(br.readLine());

            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());

            A[a].add(b);
            A[b].add(a);
        }
        int count = 0;
        for (int i = 1; i < N + 1; i++) {
            if (!visited[i]) {
                count++;
                DFS(i);
            }
        }
        System.out.println(count);
    }

    private static void DFS(int v) {
        if (visited[v]) {
            return;
        }
        visited[v] = true;
        for (int n : A[v]) {
            if (!visited[n]) {
                DFS(n);
            }
        }
    }
}
