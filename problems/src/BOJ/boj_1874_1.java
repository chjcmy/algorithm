package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

/*
    임의의 수열을 스택에 넣었다가 출력하는 방식으로 오름차순 수열을 출력할 수 있는지 확인 하고, 출력할 수 있다면
    push 와 pop 연산을 어떤 순서로 수행해야 하는지를 알아내는 프로그램을 작성해 보자.

    1. 수열의 개수를 저장한다
    2. 수열의 개수만큼 값을 받는다.
    3. 오름 차순으로 비교 해야되기 때문에 수열의 원소 하나를 저장 할수 있는 num 이라는 변수를 0으로 지정한다
    4. for 문을 통하여 수열처음 부터 끝까지 반복 시킨다.
    5. 반복문 안에는 num 보다 수열의 원소가 클 경우 num++을 하고 수열안에 넣어 주고 "+" 을 버퍼안에 추가 해준다
    6. 그 후 stack 안에 있는 값을 꺼낸뒤에 string버퍼 안에 "-" 를 추가해준다
    7. 만약 스택의 마지막 값이 추가 하는 값 보다 클 경우 "NO"를 출력해준다
    8. 아닐 경우 에는 string 버퍼를 출력해준다
 */

public class boj_1874_1 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1874.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuffer bf = new StringBuffer();

        int X = Integer.parseInt(br.readLine());

        int[] arr = new int[X];

        for (int i = 0; i < X; i++) {
            arr[i] = Integer.parseInt(br.readLine());
        }

        Stack<Integer> stack = new Stack<>();

        int num = 1;

        boolean result = true;

        for (int su : arr) {
            if (su >= num) {
                while (su >= num) {
                    stack.push(num++);
                    bf.append("+\n");
                }
                stack.pop();
                bf.append("-\n");
            } else {
                int n = stack.pop();
                if (n > su) {
                    System.out.println("NO");
                    result = false;
                    break;
                } else {
                    bf.append("-\n");
                }
            }
        }
        if (result) {
            System.out.println(bf);
        }
    }
}
