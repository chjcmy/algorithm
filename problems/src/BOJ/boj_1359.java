package BOJ;

/*
        복권을 뽑는다.
        1 부터 N 까지 있고 다른 M개 의 수를 고른다

        알기로는 경우의 수 문제 일거 같다는 생각이 든다


 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_1359 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1359.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        int k = Integer.parseInt(st.nextToken());

        double res = 0.0;
        double p = Combination(n, m);

        while (m >= k) {
            if (n - m < m - k) {
                k++;
                continue;
            }

            double c = Combination(m, k) * Combination(n - m, m - k);

            res += c / p;
            k++;
        }
        System.out.println(res);
    }

    private static long Combination(int n, int r) {

        int p = 1;
        int c = 1;
        while (r > 0) {
            c *= n--;
            p *= r--;
        }
        return c / p;
    }
}
