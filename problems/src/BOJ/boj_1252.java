package BOJ;

/*
    두 개의 이진수를 입력 받아 이를 더하는 프로그램을 작성하시오.

    1. 한줄에 a b 값을 받는다.
    2. 이진수를 10진수로 바꿔 준다
    3. 더한뒤에 2진수로 바꿔주면서 출력해준다.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class boj_1252 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1252.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        BigInteger a = new BigInteger(st.nextToken(), 2);
        BigInteger b = new BigInteger(st.nextToken(), 2);

        BigInteger sum = a.add(b);

        String result = sum.toString(2);

        System.out.println(result);
    }
}
