package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/*
    1번 부터 n번 까지 n명의 사람이 원을 이루면서 앉아 있고, 양의 정수 k가 주어진다.(k 는 n 보다 작거나 같다)
    한 사람이 제거되면 남은 사람들로 이루어진 원을 따라 이 과정을 계속해 나간다.
    이과정은 n명의 사람이 모두 제거될 때까지 계속 된다.
    원에서 사람들이 제거되는 순서를 (N, K)- 요세푸스 순열이라고한다.
    예를 들어 (7, 3)- 요세푸스 순열은 <3, 6, 2, 7, 5, 1, 4> 이다.

    1. N의 수를 받는다
    2. K의 수를 받는다
    3. 링크드 리스트를 이용하여 정수 큐 링크드리스트 를 만들어서 1 부터 n 까지 채운다
    4. 0에서 k-1 까지 큐에서 뺀 후 다시 집어 넣어준다
    5. k 번째 수는 빼서 출력해준다
    6. 큐 링크드 리스트가
 */

public class boj_1158 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1158.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        int N = Integer.parseInt(st.nextToken());

        int K = Integer.parseInt(st.nextToken());

        Queue<Integer> q = new LinkedList<>();

        for (int i = 1; i <= N; i++) {
            q.offer(i);
        }

        System.out.print("<");

        while (q.size() != 1) {

            for (int i = 0; i < K - 1; i++) {
                q.offer(q.poll());
            }

            System.out.print(q.poll() + ", ");
        }

        System.out.print(q.poll() + ">");
        
    }
}
