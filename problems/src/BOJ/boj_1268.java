package BOJ;

/*
    1학년 부터 5학년 까지 지내 오면서 한번이라도 같은 반이어떤 사람이 가장 많은 학생을 임시 반장으로 정하는 프로그램을 만들어라

    방법: 1학년 때부터 5학년 때까지 같은 번호가 있을 경우 비교를 하고 그 학생에 1을 더해준다.
         다 끝났을 경우 하나 씩 비교하여 제일 큰 값을 가지고 있는 학생을 출력 해준다

    1. T 를 받아 저장을 한다.
    1. 1. T만큼 배열을 만든다
    2. T 만큼 5 만큼 이중배열을 만들고 T에 값을 1 에서 5 까지 저장 시킨다.
    3. for 문을 이용해 이중 배열에 x 축 배열을 비교하면서 같은 값이 있을 경우 비교를 시작 한다.
    4. 비교시 비교를 한곳은 비교를 하지 않게 만든다.
    5. 둘이 같은 값일 경우 둘 다 배열에 값을 올려준다
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class boj_1268 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1268.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        int[][] classArr = new int[T][5];

        int maxPerson = 0;

        int result = 0;

        for (int i = 0; i < classArr.length; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < classArr[i].length; j++) {
                classArr[i][j] = Integer.parseInt(st.nextToken());
            }
        }

        for (int i = 0; i < T; i++) {
            Set<Integer> sameClass = new HashSet<>();

            for (int j = 0; j < 5; j++) {
                for (int k = 0; k < T; k++) {
                    if (classArr[i][j] == classArr[k][j] && k != i) {
                        sameClass.add(k);
                    }
                }

                if (maxPerson < sameClass.size()) {
                    maxPerson = sameClass.size();
                    result = i;
                }
            }
        }

        System.out.println(result + 1);
    }
}
