package BOJ;

/*
    피보나치수열

    1. 0 일경우 0
    2. 1 일경우 1
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class boj_2748 {

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2748.txt"));

        long[] fibo = new long[91];

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int max = Integer.parseInt(br.readLine());

        fibo[0] = 0;
        fibo[1] = 1;
        fibo[2] = 2;

        for (int i = 2; i <= max; i++) {
            fibo[i] = fibo[i - 1] + fibo[i - 2];
        }
        System.out.println(fibo[max]);

    }
}
