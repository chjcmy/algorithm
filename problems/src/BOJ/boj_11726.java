package BOJ;

/*
    2 x n 타일링

    1. 최대 값 만큼 배열을 만든다
    2. n 값을 받는다
    3. n 과 n -1 값으로 점화식을 만들어 값을 찾는다
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class boj_11726 {

    public static void main(String[] args) throws IOException {

        System.setIn(new FileInputStream("./src/boj_text/11726.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(br.readLine());

        int[] array = new int[1001];


        array[1] = 1;
        array[2] = 2;

        for (int i = 3; i < a + 1; i++) {
            array[i] = (array[i - 1] + array[i - 2]) % 10007;
        }
        System.out.println(array[a]);
    }

    class Car {

    }
}
