package BOJ;

/*
    쩰리는 새로운 점프게임을 만들고 싶다.
    #1. 칸수가 같은 정사각형 내에서 움직일수 있다. 쩰리가 바닥으로 떨어지면 즉시 게임에서 패배 하게 된다
    #2. 쩰리의 출발 점은 정사각형 가장 왼쪽 가장 위의 칸이다.
    #3. 쩰리가 가장 오른 쪽, 가장 아래 칸에 도달 하는 순간, 쩰리의 승리로 게임은 종류 된다
    #4. 쩰리가 한번에 이동할 수 있는 칸의 수는, 현재 밟고 있는 칸에 쓰여져 있는 수이다.
        칸에 쓰여 있는 수를 초과 하거나 미만으로 이동할수 없다.

     1. 맵의 크기를 받는다.
     2. 맵의 크기로 이차원 배열을 받는다.
     3. 맵의 위치마다 옮겨갈수 있는 숫자를 입력 받아 배열 위치에 값을 저장 시킨다
     4. dfs 알고리즘으로 아래쪽, 오른쪽으로만 움직일 있게 만든다.
     5. 만약 -1 을 만났을 경우 result를 true 끝낸다.
     6. 만약 y, x가 맵의 크기를 넘었을 경우 끝낸다.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_16173 {

    static boolean result;

    static int[] dr = {1, 0};
    static int[] dc = {0, 1};
    static int N;
    static int[][] mapArr;
    static boolean[][] visited;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/16173.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        N = Integer.parseInt(br.readLine());

        mapArr = new int[N][N];
        visited = new boolean[N][N];


        result = false;

        for (int i = 0; i < N; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < N; j++) {
                mapArr[i][j] = Integer.parseInt(st.nextToken());
            }
        }

        DFS(0, 0);

        if (result) {
            System.out.println("HaruHaru");
        } else {
            System.out.println("Hing");
        }
    }

    static void DFS(int a, int b) {

        int jump = mapArr[a][b];

        if (mapArr[a][b] == -1) {
            result = true;
            return;
        }

        for (int i = 0; i < 2; i++) {

            int row = a + dr[i] * jump;
            int col = b + dc[i] * jump;

            if (row < 0 || col < 0 || row >= N || col >= N) continue;
            if (visited[a][b]) continue;

            visited[a][b] = true;
            DFS(row, col);
        }
    }
}
