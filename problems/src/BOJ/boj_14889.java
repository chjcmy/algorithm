package BOJ;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_14889 {

    static int n;
    static int[][] ability;
    static int[] team;
    static int minValue = Integer.MAX_VALUE;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        n = Integer.parseInt(br.readLine());
        ability = new int[n][n];
        for (int i = 0; i < n; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < n; j++) {
                ability[i][j] = Integer.parseInt(st.nextToken());
            }
        }
        team = new int[n];
        dfs(0, 0);
        System.out.println(minValue);
    }

    static void dfs(int depth, int start) {
        if (depth == n / 2) {
            int team1 = 0;
            int team2 = 0;
            for (int i = 0; i < n; i++) {
                if (team[i] == 1) {
                    team1 += ability[i][i];
                } else {
                    team2 += ability[i][i];
                }
            }
            minValue = Math.min(minValue, Math.abs(team1 - team2));
            return;
        }
        for (int i = start; i < n; i++) {
            team[i] = 1;
            dfs(depth + 1, i + 1);
            team[i] = 0;
        }
    }
}
