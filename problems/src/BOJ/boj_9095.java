package BOJ;

/*
    정수 x를 1,2,3으로 구할수 있는 갯수를 구하라

    dp 문제 점화식을 찾아야 된다
    1. 테스트 하는 개수를 받는다
    2. 받은 테스트 개수 만큼 반복을 시킨다
    3. 테스트 값을 받는다
    4. 11의 배열을 만든다
    5. 처음 1, 2, 3 값을 찾는다
    6. 점화식으로 값을 찾는다
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class boj_9095 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/9095.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int test_count = Integer.parseInt(br.readLine());

        int[] count_array = new int[11];

        count_array[1] = 1;
        count_array[2] = 2;
        count_array[3] = 4;

        for (int i = 4; i < 11; i++) {
            count_array[i] = count_array[i - 1] + count_array[i - 2] + count_array[i - 3];
        }
        for (int i = 0; i < test_count; i++) {
            int k = Integer.parseInt(br.readLine());
            System.out.println(count_array[k]);
        }
    }
}
