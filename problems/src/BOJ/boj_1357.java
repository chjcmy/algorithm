package BOJ;

/*
    두개의 숫자를 받아 두개의 숫자를 거꾸로 뒤집은뒤에 더 한 후 그 숫자를 뒤집어라

    1. 두개의 숫자를 문자열로 받아 뒤집는다
    2. 문자열이었던 숫자를 두개의 정수형으로 바꾼다
    3. 바꾼 숫자를 두개를 더해준다.
    4. 바꾼 숫자를 뒤집어서 출력해준다.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_1357 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1357.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        StringBuffer a = new StringBuffer(st.nextToken());
        StringBuffer b = new StringBuffer(st.nextToken());

        int reverseA = Integer.parseInt(String.valueOf(a.reverse()));
        int reverseB = Integer.parseInt(String.valueOf(b.reverse()));

        int result = reverseA + reverseB;

        int reverseResult = Integer.parseInt(new StringBuffer(String.valueOf(result)).reverse().toString());

        System.out.println(reverseResult);
    }
}
