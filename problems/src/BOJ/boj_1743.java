package BOJ;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 음식 피하기
   USER: choi
   DATE-TIME: 8/10/23_11:22 AM
   
   1.
   2.
   3.
   4.
   5.
*/
public class boj_1743 {
	static int[][] map;
	static boolean[][] bool_map;
	static int[] move = {1, -1, 1, -1};
	static int result, r, c, sum;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		StringTokenizer st;

		st = new StringTokenizer(br.readLine());
		result = 0;

		r = Integer.parseInt(st.nextToken()) + 1;
		c = Integer.parseInt(st.nextToken()) + 1;
		int n = Integer.parseInt(st.nextToken());

		map = new int[r][c];
		bool_map = new boolean[r][c];

		for (int i = 0; i < n; i++) {
			st = new StringTokenizer(br.readLine());

			map[Integer.parseInt(st.nextToken())][Integer.parseInt(st.nextToken())] = 1;
		}

		for (int i = 1; i < r; i++) {
			for (int j = 1; j < c; j++) {

				if (!bool_map[i][j] && map[i][j] == 1) {
					sum = 1;
					bfs(i, j);
				}
				bool_map[i][j] = true;
				sum = 0;
			}
		}
		System.out.println(result);
	}

	private static void bfs(int i, int j) {
		bool_map[i][j] = true;

		if (i + 1 < r && map[i + 1][j] == 1 && !bool_map[i + 1][j]) {
			sum++;
			bfs(i + 1, j);
		}
		if (i - 1 > 0 && map[i - 1][j] == 1 && !bool_map[i - 1][j]) {
			sum++;
			bfs(i - 1, j);
		}
		if (j + 1 < c && map[i][j + 1] == 1 && !bool_map[i][j + 1]) {
			sum++;
			bfs(i, j + 1);
		}
		if (j - 1 > 0 && map[i][j - 1] == 1 && !bool_map[i][j - 1]) {
			sum++;
			bfs(i, j - 1);
		}
		if (result < sum) {
			result = sum;
		}
	}
}
