package BOJ;

/*
    구간의 합을 구하라
    122333444455555

    1.

    1. 구간 값 두개를 받는다
    2. sum 이라는 변수를 선언하고 처음 부터 끝까지 값 구하기
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_1292 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1292.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int min = Integer.parseInt(st.nextToken());
        int max = Integer.parseInt(st.nextToken());

        int cnt = 1;
        int sum = 0;

        int[] arr = new int[1001];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (cnt == 1001) break;
                arr[cnt] = i;
                cnt++;
            }
        }

        while (min <= max) {
            sum += arr[min];
            min++;
        }

        System.out.println(sum);
    }
}
