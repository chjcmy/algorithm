package BOJ;

/*
    n장의 카드가 있다. 각각의 카드는 차례로 1 에서 n 까지의 번호가 붙어 있다, 1번 카드가 가장 위, n 번 카드가 가장 아래인 상태로 놓여 있다.
    먼저 가장 위에 있는 카드를 바닥에 버린다.

    1. n 값을 받는다.
    2. for 문을 이용하여 n부터 1 까지 큐에 추가한다.
    3. 처음 수를 버리고 다음 수를 맨 뒤쪽에 추가한다.
    4. 만약 큐에 크기가 1 일 경우 마지막 카드를 result 에 추가하고 출력한다.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class boj_2164 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2164.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(br.readLine());

        Queue<Integer> queue = new LinkedList<>();

        for (int i = 1; i <= n; i++) {
            queue.add(i);
        }
        while (queue.size() > 1) {
            queue.poll();
            queue.add(queue.poll());
        }
        System.out.println(queue.poll());
    }
}
