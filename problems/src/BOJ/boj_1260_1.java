package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class boj_1260_1 {

    static ArrayList<Integer>[] graph;

    static boolean[] visite;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1260.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int maxGraph = Integer.parseInt(st.nextToken());

        int setTime = Integer.parseInt(st.nextToken());

        int startNum = Integer.parseInt(st.nextToken());

        graph = new ArrayList[maxGraph + 1];

        for (int i = 0; i < graph.length; i++) {
            graph[i] = new ArrayList<>();
        }

        for (int i = 0; i < setTime; i++) {
            st = new StringTokenizer(br.readLine());

            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());

            graph[a].add(b);
            graph[b].add(a);
        }

        for (int i = 0; i < graph.length; i++) {
            Collections.sort(graph[i]);
        }
        visite = new boolean[maxGraph + 1];
        DFS(startNum);
        visite = new boolean[maxGraph + 1];
        BFS(startNum);
    }

    static void DFS(int a) {
        visite[a] = true;
        System.out.print(a + " ");
        for (int i : graph[a]) {
            if (!visite[i]) {
                DFS(i);
            }
        }
    }

    static void BFS(int a) {
        LinkedList<Integer> queue = new LinkedList<>();
        queue.add(a);
        visite[a] = true;

        while (!queue.isEmpty()) {
            int nowA = queue.poll();

            System.out.print(nowA + " ");
            for (int i : graph[nowA]) {
                visite[i] = true;
                queue.add(i);
            }
        }

    }
}
