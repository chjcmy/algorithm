package BOJ;

/*
    n 과 m 크기의 배열로 표현되는 미로가 있다.
    1,1 에서 출발 하게 되고 n, m의 위치로 이동할때 지나야 하는 최소의 칸수를 구하는 프로그램을 만드시오

    칸수를 넣을수 있는 이차원 배열을 만들면서 참거짓의 이차원 배열을 만들어서 스택을 통해서 만약 갈수 있는곳이라면 가고
    가지못하는 칸이나 참거짓 배열에서 false 일경우 가지 않는다
    원 하는 위치에 왔을 경우 카운트 한걸 넣고 프로그램을 끝낸다
    */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class boj_2178 {

    static int N;
    static int M;

    static int[][] mazeMap;
    static boolean[][] booleansMaze;
    static int[] moveX = {0, 1, 0, -1};
    static int[] moveY = {1, 0, -1, 0};

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2178.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());
        N = Integer.parseInt(st.nextToken());
        M = Integer.parseInt(st.nextToken());

        mazeMap = new int[N + 1][M + 1];
        booleansMaze = new boolean[N + 1][M + 1];

        for (int i = 1; i < N + 1; i++) {
            st = new StringTokenizer(br.readLine(), "", true);
            String string = st.nextToken();
            for (int j = 1; j < M + 1; j++) {
                mazeMap[i][j] = Integer.parseInt(string.substring(j - 1, j));
            }
        }

        BFS(1, 1);
        System.out.println(mazeMap[N][M]);
    }

    static void BFS(int y, int x) {
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{x, y});

        while (!queue.isEmpty()) {
            int[] now = queue.poll();
            booleansMaze[x][y] = true;
            for (int i = 0; i < 4; i++) {
                int plusX = now[0] + moveX[i];
                int plusY = now[1] + moveY[i];
                if (plusX >= 1 && plusY >= 1 && plusX < N + 1 && plusY < M + 1) {
                    if (mazeMap[plusX][plusY] != 0 && !booleansMaze[plusX][plusY]) {
                        booleansMaze[plusX][plusY] = true;
                        mazeMap[plusX][plusY] = mazeMap[now[0]][now[1]] + 1;
                        queue.add(new int[]{plusX, plusY});
                    }
                }
            }
        }
    }
}
