package BOJ;

import java.util.Scanner;

public class boj_12865 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(); // 물품의 수
        int K = sc.nextInt(); // 최대 무게

        int[] weights = new int[N + 1];
        int[] values = new int[N + 1];

        for (int i = 1; i <= N; i++) {
            weights[i] = sc.nextInt();
            values[i] = sc.nextInt();
        }

        int[][] dp = new int[N + 1][K + 1];

        for (int i = 1; i <= N; i++) {
            for (int j = 0; j <= K; j++) {
                if (weights[i] > j) { // 현재 물품을 가방에 넣을 수 없는 경우
                    dp[i][j] = dp[i - 1][j];
                } else { // 현재 물품을 가방에 넣을 수 있는 경우
                    dp[i][j] =
                            Math.max(dp[i - 1][j], dp[i - 1][j - weights[i]] + values[i]);
                }
            }
        }

        System.out.println(dp[N][K]);
    }
}
