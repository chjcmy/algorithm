package BOJ;

/*
    작은 박스가 n개 이고 l, w, h 인 직육면체 박스에 모두 넣으려고 한다
    모든 작은 박스는 큰 박스 안에 있어야 하고, 작은 박스의 변은 큰 박스의 변과 평행해야 한다
    n, l, w, h 가 주어진다. 가능한 a의 최댓값을 찾는 프로그램을 작성하시오



    1. n, l, w, h 값을 순서대로 받는다
    2. start를 0으로 저장하고 end를 l, w, h에 제일 큰 값을 구한다
    3. 이분탐색을 통해 , 정육면체의 한변의 길이를 구하고 직육면체에 들어갈 수 있는 정육면체의 개수 구해 n과 비교 하시오
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_1166 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1166.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        long n = Integer.parseInt(st.nextToken());
        long l = Integer.parseInt(st.nextToken());
        long w = Integer.parseInt(st.nextToken());
        long h = Integer.parseInt(st.nextToken());

        double start = 0;
        double end = Math.max(l, Math.max(w, h));

        while (start < end) {
            double mid = (start + end) / 2;

            if ((long) (l / mid) * (long) (w / mid) * (long) (h / mid) < n) {
                if (end == mid) break;
                end = mid;
            } else {
                if (start == mid) break;
                start = mid;
            }
        }
        System.out.println(start);
    }
}
