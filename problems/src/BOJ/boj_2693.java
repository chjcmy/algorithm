package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class boj_2693 {

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2693.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int select_num = Integer.parseInt(br.readLine());

        for (int i = 0; i < select_num; i++) {
            String read_strings = br.readLine();

            int[] array_int = new int[10];
            StringTokenizer st = new StringTokenizer(read_strings);
            for (int j = 0; j < 10; j++) {
                array_int[j] = Integer.parseInt(st.nextToken());
            }
            Arrays.sort(array_int);

            System.out.println(array_int[7]);
        }
    }
}
