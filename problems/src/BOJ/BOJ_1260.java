package BOJ;

/*
     dfs 깊이 탐색
     bfs 넓이 탐색

     dfs 깊게 들어 가면서 표기
     bfs 넓게 펼친다음에 넓이 만큼 표기 한뒤에 깊이로 표기

     1. 노드개수, 에지 개수, 시작점 저장한다
     2. 어레이 리스트를 노드 개수 만큼 만든다
     3. 에지 개수 만큼 노드에 수를 추가한다
     4. 노드 개수 만큼 불리언 배열을 만든다
     5. dfs를 돌린뒤에 불리언 배열을 초기화 시킨다
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class BOJ_1260 {

    static ArrayList<Integer>[] allFriends;
    static boolean[] realFriend;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1260.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int person = Integer.parseInt(st.nextToken());
        int root = Integer.parseInt(st.nextToken());
        int startPerson = Integer.parseInt(st.nextToken());

        allFriends = new ArrayList[person + 1];


        for (int i = 0; i <= person; i++) {
            allFriends[i] = new ArrayList<>();
        }


        for (int i = 0; i < root; i++) {
            st = new StringTokenizer(br.readLine());

            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());

            allFriends[a].add(b);
            allFriends[b].add(a);
        }
        for (int i = 0; i <= person; i++) {
            Collections.sort(allFriends[i]);
        }
        realFriend = new boolean[person + 1];
        DFS(startPerson);
        System.out.println();
        realFriend = new boolean[person + 1];
        BFS(startPerson);
    }

    static void DFS(int a) {
        realFriend[a] = true;
        System.out.print(a + " ");
        for (int b : allFriends[a]) {
            if (!realFriend[b]) {
                DFS(b);
            }
        }
    }

    static void BFS(int a) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(a);
        realFriend[a] = true;

        while (!queue.isEmpty()) {
            int now_a = queue.poll();

            System.out.print(now_a + " ");
            for (int i : allFriends[now_a]) {
                if (!realFriend[i]) {
                    realFriend[i] = true;
                    queue.add(i);
                }
            }
        }
    }
}
