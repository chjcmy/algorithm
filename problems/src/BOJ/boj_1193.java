package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/*
    분수 배열에 순서에 맞춰서 T 위치에 있는 분수를 구하여라

    1. T 를 저장 한다
    2. crossInt 와 perv_cross_count 를 1, 0 으로 생성시킨다.
    3. T 보다 crossInt + perv_cross_count 보다 클경우 끈낸다
    4. perv
 */

public class boj_1193 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1193.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        int crossInt = 1, pervCrossSum = 0;

        while (true) {
            if (T <= crossInt + pervCrossSum) {
                if (crossInt % 2 == 1) {
                    System.out.println((crossInt - (T - pervCrossSum - 1)) + "/" + (T - pervCrossSum));
                    break;
                } else {
                    System.out.println((T - pervCrossSum) + "/" + (crossInt - (T - pervCrossSum - 1)));
                    break;
                }
            } else {
                pervCrossSum += crossInt;
                crossInt++;
            }
        }
    }
}
