package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * T가 주어진다. 배추 밭의 가로길이 M과 세로길이 N, 그리고 배추가 심어져 있는 위치의 개수 K이 주어진다.
 * 그 다음 K줄에는 배추의 위치 X, Y 가 주어진다.
 * 두배추의 위치가 같은 경우는 없다.
 * <p>
 * 한마리만 있으면 되므로 서로 인접해있는 배추들이 몇 군데에 퍼져 있는지 조사하면 총 몇 마리의 지렁이가 필요한지 알 수 있다.
 * <p>
 * 풀이 : 모아져 있는 배추에는 지렁이 카운트에 1을 더해준다.
 * 0. 동.서.남.북 으로 갈수 있는 배열을 만든다
 * 1. T를 받아 저장한다
 * 2. T 만큼 테스트를 진행한다
 * 3. 행, 열을 저장 한뒤 참거짓 이차원 배열을 만든다.
 * 4. 들어온 배추 만큼 참거짓 이차원 배열 에 true로 저장한다
 * 5. 이차원 배열을 Y부터
 */

public class boj_1012 {

    static int n, m, k;
    static int[][] board;
    static int[][] visited;
    static int[] dx = {1, 0, -1, 0};
    static int[] dy = {0, 1, 0, -1};

    public static void main(String[] args) throws IOException {

        System.setIn(new FileInputStream("./src/boj_text/1012.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());


        for (int l = 0; l < T; l++) {
            int ans = 0;
            StringTokenizer st = new StringTokenizer(br.readLine());
            m = Integer.parseInt(st.nextToken());
            n = Integer.parseInt(st.nextToken());
            k = Integer.parseInt(st.nextToken());
            board = new int[n][m];
            visited = new int[n][m];

            for (int j = 0; j < k; j++) {
                st = new StringTokenizer(br.readLine());
                int a = Integer.parseInt(st.nextToken());
                int b = Integer.parseInt(st.nextToken());
                board[b][a] = 1;
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (visited[i][j] == 0 && board[i][j] == 1) {
                        bfs(i, j);
                        ans++;
                    }
                }
            }
            System.out.println(ans);
        }
    }

    public static void bfs(int a, int b) {
        Queue<int[]> q = new LinkedList<>();
        q.add(new int[]{a, b});
        visited[a][b] = 1;
        while (!q.isEmpty()) {
            int[] cur = q.poll();
            for (int i = 0; i < 4; i++) {
                int nx = dx[i] + cur[0];
                int ny = dy[i] + cur[1];
                if (nx < 0 || ny < 0 || nx >= n || ny >= m) continue;
                if (board[nx][ny] == 0 || visited[nx][ny] == 1) continue;
                visited[nx][ny] = 1;
                q.add(new int[]{nx, ny});
            }
        }
    }
}