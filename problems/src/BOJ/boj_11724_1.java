package BOJ;

/*
    1. M 과 N 을 받아 변수에 저장한다
    2. M 만큼 이진 어레이 리스트를 만든다
    3. N 만큼 숫자를 받아 이진어레이에 저장한다.
    4. 불리언 어레이를 만들어서 FALSE를 만든다
    5. for문으로 boolean_arr[i]가 false 일 경우 cnt에 1 을 추가 하고 dfs 로 계솔 돌린다.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class boj_11724_1 {
    static ArrayList<Integer>[] arrayLists;
    static boolean[] booleans;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/11724.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int M = Integer.parseInt(st.nextToken());
        int N = Integer.parseInt(st.nextToken());

        arrayLists = new ArrayList[M + 1];
        booleans = new boolean[M + 1];

        for (int i = 0; i < M + 1; i++) {
            arrayLists[i] = new ArrayList<Integer>();
        }

        for (int i = 0; i < N; i++) {
            st = new StringTokenizer(br.readLine());
            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());

            arrayLists[a].add(b);
            arrayLists[b].add(a);
        }

        int cnt = 0;

        for (int i = 1; i < M + 1; i++) {
            while (!booleans[i]) {
                cnt++;
                DFS(i);
            }
        }
        System.out.println(cnt);
    }

    private static void DFS(int a) {
        if (booleans[a]) {
            return;
        }

        booleans[a] = true;

        for (int b : arrayLists[a]) {
            DFS(b);
        }
    }
}
