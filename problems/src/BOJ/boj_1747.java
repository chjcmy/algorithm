package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * 어떤수 와 그수의 숫자 순서를 뒤집은 수가 일치하는 수를 패린드롬이라 부른다. 예를 들어 79,197rhk 324423 등이 팰린드롬 수이다
 * N보다 크거나 같고, 소수이면서 패린드롬인 수중에서, 가장 작은 수를 구하는 프로그램을 작성하시오.
 *
 * 1. T 를 정수 변수에 저장한다.
 * 2. T 부터 1000000 반복 시킨다.
 * 3. 만약 그 수가 소수 이면 그 수를 스트링으로 만들고 reverse를 하고 맞다면 정지시킨다.
 */

public class boj_1747 {

    static int T;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1747.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        T = Integer.parseInt(br.readLine());

        if (T == 1){
            System.out.println(2);
            return;
        }

        for (int i = T; ; i++){
            if (isPalind(i) && isPrime(i)){
                System.out.println(i);
               break;
            }
        }
    }
    public static boolean isPrime(int x){
        for (int i = 2; i <= Math.sqrt(x); i++){
            if (x % i == 0){
                return false;
            }
        }
        return true;
    }
    public static boolean isPalind(int x) {
        String strX = Integer.toString(x);
        int cnt = strX.length();
        for (int i = 0; i <= cnt / 2; i++){
            if (strX.charAt(i) != strX.charAt(cnt-i-1)){
                return false;
            }
        }
        return true;
    }
}