package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/*
    boat crew ecology together economy tower type crucial hole elephant young basic

    트리에서 임의의 두점 사이의 거리 중 가장 긴 것을 구하는 프로그램을 만들어라

    트리의 정점은 1 이고 v 까지 있다 먼저 정점 번호가 주어지고 연결된다

    1. 왔다 갔다 한것을 증명 하기 위해 visited라는 참거짓 배열을 만든다
    2. 노드간의 최대 거리를 저장 하기 위해 저장 하는 배열을 만든다
    3. 그래프 데이터를 저장 할수 있는 인접 리스트 를 만든다
    4. 노드 개수를 받는 변수를 만든다
    5. A 인접 리스트에 그래프 데이터를 저장을 한다
    6. nodeRange 라는 거리 배열을 N+1 만큼 초기화 시킨다
    7. visited 라는 참거짓 배열을 N+1 로 초기화 시킨다
    8. 1로 시작하는 bfs 함수를 실행 시킨다
    9. 제일 긴 거리를 받기위해 Max 라는 변수를 만든다
    10. 그 후 nodeRange 라는 배열에서 가장 큰 값으로 다시 시작점을 설정 시키게 만든다
    11. for문을 돌면서 제일 큰수를 Max에 저장 시킨뒤
    12. nodeRage 배열과 visited 배열을 초기 화 한다
    13. 그 후 bfs에 Max를 넣어서 다시 돌린뒤 nodeRange를 정렬 시킨뒤
    14. nodeRange의 N 값을 출력한다.
 */
public class boj_1167 {
    static boolean[] visited;
    static int[] nodeRange;
    static ArrayList<Edge>[] A;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1167.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int N = Integer.parseInt(br.readLine());

        A = new ArrayList[N + 1];

        for (int i = 1; i <= N; i++) {
            A[i] = new ArrayList<>();
        }

        for (int i = 0; i < N; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int S = Integer.parseInt(st.nextToken());
            while (true) {
                int E = Integer.parseInt(st.nextToken());
                if (E == -1) {
                    break;
                }
                int V = Integer.parseInt(st.nextToken());
                A[S].add(new Edge(E, V));
            }
        }
        nodeRange = new int[N + 1];
        visited = new boolean[N + 1];
        BFS(1);
        int Max = 1;
        for (int i = 2; i <= N; i++) {
            if (nodeRange[Max] < nodeRange[i]) {
                Max = i;
            }
        }
        nodeRange = new int[N + 1];
        visited = new boolean[N + 1];
        BFS(Max);
        Arrays.sort(nodeRange);
        System.out.println(nodeRange[N]);
    }

    static void BFS(int index) {
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(index);
        visited[index] = true;
        while (!queue.isEmpty()) {
            int nowNode = queue.poll();
            for (Edge i : A[nowNode]) {
                int e = i.e;
                int v = i.value;
                if (!visited[e]) {
                    visited[e] = true;
                    queue.add(e);
                    nodeRange[e] = nodeRange[nowNode] + v;
                }
            }
        }
    }
}

class Edge {
    int e;
    int value;

    public Edge(int e, int value) {
        this.e = e;
        this.value = value;
    }
}
