package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/*
    1. a 라는 값을 받는다
    2. a 값이 3 으로 나누어 진다면 나누어진 값을 이용 하여 dp 를 다시도 돌리게 만든다
    3. 아닐경우 a 값을 2로 나누어 나머지 값이 없을경우 dp 로 다시 돌린다
    4. 그것도 안될 경우 1을 빼고 dp 를 다시 돌린다
 */


public class boj_1463 {

    static int N;
    static int[] D;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1463.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        for (int i = 0; i < 2; i++) {
            N = Integer.parseInt(br.readLine());
            D = new int[N + 1];
            D[1] = 0;
            for (int j = 2; j <= N; j++) {
                D[j] = D[j - 1] + 1;
                if (j % 2 == 0) D[j] = Math.min(D[j], D[j / 2] + 1);
                if (j % 3 == 0) D[j] = Math.min(D[j], D[j / 3] + 1);
            }
            System.out.println(D[N]);
        }
    }
}
