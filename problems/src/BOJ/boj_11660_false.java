package BOJ;

/*
  n x n 크기의 표에 값이 채워져 있다
  테스트 케이스 값이 정해지고 테스트 케이스 마다 처음 시작 값과 나중 시작 값이 채워져 있다.
  정해진 값에 맞춰서 구하여라

  1. 2차원 배열의 크기를 받고, 테스트 케이스 갯수를 구하여라
  2. 2차원 배열 크기 만큼 값을 받는다
  3. 테스트 케이스 만큼 값을 받는다
  4. 처음 시작 하는 테스트 와 끝나는 테스트 크기를 받는다
  5. x 값 부터 테스트 시작 부터 끝날때 까지 계산을 한다
  6. y 값 부터 테스트 시작 부터 끝날때 까지 계산을 한다
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_11660_false {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/11660.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int arrSize = Integer.parseInt(st.nextToken());
        int T = Integer.parseInt(st.nextToken());

        int[][] arr = new int[arrSize + 1][arrSize + 1];


        for (int i = 1; i < arr.length; i++) {

            st = new StringTokenizer(br.readLine());

            for (int j = 1; j < arr.length; j++) {
                arr[i][j] = Integer.parseInt(st.nextToken());
            }
        }

        for (int i = 0; i < T; i++) {

            st = new StringTokenizer(br.readLine());

            int result = 0;

            int x1 = Integer.parseInt(st.nextToken());
            int y1 = Integer.parseInt(st.nextToken());
            int x2 = Integer.parseInt(st.nextToken());
            int y2 = Integer.parseInt(st.nextToken());

            for (int j = x1; j <= x2; j++) {
                for (int k = y1; k <= y2; k++) {
                    result += arr[j][k];
                }
            }
            System.out.println(result);
        }
    }
}
