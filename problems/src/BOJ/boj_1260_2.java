package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class boj_1260_2 {

    private static ArrayList<Integer>[] graph;

    private static boolean[] boolean_array;


    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1260.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int max_num = Integer.parseInt(st.nextToken());
        int get_num = Integer.parseInt(st.nextToken());
        int start_num = Integer.parseInt(st.nextToken());

        graph = new ArrayList[max_num + 1];

        for (int i = 0; i < graph.length; i++) {
            graph[i] = new ArrayList<>();
        }

        for (int i = 0; i < get_num; i++) {
            st = new StringTokenizer(br.readLine());
            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());

            graph[a].add(b);
            graph[b].add(a);
        }
        boolean_array = new boolean[max_num + 1];
        DFS(start_num);
        System.out.println();
        boolean_array = new boolean[max_num + 1];
        BFS(start_num);
    }

    private static void DFS(int a) {
        boolean_array[a] = true;
        System.out.print(a + " ");
        for (int b : graph[a]) {
            if (!boolean_array[b]) DFS(b);
        }
    }

    private static void BFS(int a) {
        LinkedList<Integer> queue = new LinkedList();

        queue.add(a);
        boolean_array[a] = true;
        while (!queue.isEmpty()) {

            int c = queue.poll();
            System.out.print(c + " ");
            for (int b : graph[c]) {
                if (!boolean_array[b]) {
                    boolean_array[b] = true;
                    queue.add(b);
                }
            }
        }
    }
}
