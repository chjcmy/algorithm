package BOJ;

/*
    분자, 분모가 주어지고, 소수점 n 번 째 자리가 무엇인지 구하여라

    1.분자를 a 저장하고 분모를 b에 저장한다.
    2.구하고 싶은 소수점 자리 수를 n에 저장한다.
    3.분자 분모를 통해 계산을 한 뒤 나온 값을 저장한다.
    4.나온 값에 원하는 만큼 자른다

 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_1312 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1312.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int a = Integer.parseInt(st.nextToken());
        int b = Integer.parseInt(st.nextToken());
        int n = Integer.parseInt(st.nextToken());
        int result = a % b;

        for (int i = 0; i < n - 1; i++) {
            result *= 10;
            result %= b;
        }
        result *= 10;
        System.out.println(result / b);

//        float result = a / b;
//
//        while (result < Math.pow(10, n)) {
//            result *= 10;
//        }
//
//        String string = String.format("%.0f", Math.floor(result));
//        char[] chars = string.toCharArray();
//        System.out.println(chars[chars.length - 1]);

    }
}
