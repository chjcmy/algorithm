package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
    n개수의 수 a1, ... , a2 가 주어졌을 때 연속된 부분의 합이 M으로 나누어 떨어지는 구간의 개수를 구하는 프로그램을 작성하시오
    ai + ... + aj 의 합이 M으로 나누어 떨어지는 (i , j) 쌍의 개수를 구하시오

    1. n 입력 받기
    2. m 입력 받기
    3. s 선언하기
    4. c 선언하기
    5. 합 배열 저장
    6. 합 배열을 m 으로 나눈 나머지값
    7. 0일때 정답 1 증가
    8.
 */

public class boj_10986 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/10986.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        long[] S = new long[n];
        long[] C = new long[m];
        long answer = 0;
        st = new StringTokenizer(br.readLine());
        S[0] = Integer.parseInt(st.nextToken());
        for (int i = 1; i < n; i++) {
            S[i] = S[i - 1] + Integer.parseInt(st.nextToken());
        }
        for (int i = 0; i < n; i++) {
            int remainder = (int) (S[i] % m);
            if (remainder == 0) answer++;
            C[remainder]++;
        }
        for (int i = 0; i < m; i++) {
            if (C[i] > 1) {
                answer = answer + (C[i] * (C[i] - 1) / 2);
            }
        }
        System.out.println(answer);
    }
}
