package BOJ;

/*
    이차원 배열을 만들고 일차원 배열에는 크기 2 로 만든다
    그 후 학년별 남,여 학생 방수를 인원으로 나눈뒤에 만약 나머지가 있을경우에는 1을 더한다


 */


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_13300 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/13300.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int count_person = Integer.parseInt(st.nextToken());
        int size_room = Integer.parseInt(st.nextToken());
        int[][] person = new int[7][2];
        int counter = 0;
        for (int i = 0; i < count_person; i++) {
            st = new StringTokenizer(br.readLine());
            int sex = Integer.parseInt(st.nextToken());
            int grade = Integer.parseInt(st.nextToken());

            person[grade][sex]++;
        }

        for (int i = 1; i <= 1; i++) {
            int add_room = person[i][1] % size_room;
            counter += add_room;
            if (person[i][1] / size_room > 0) {
                counter++;
            }

        }

        System.out.println(counter);
    }
}
