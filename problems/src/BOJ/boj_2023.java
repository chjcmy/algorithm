package BOJ;

/*
    n 자리 소수 구하기
    1. n의 자리수를 받는 다
    2. dfs 를 이용 하여 stack에 추가 한다
    3. stck 크기가 n 만큼 사이즈가 커졌을 경우 소수 인지 확인 한다
    4. 소수 일 경우 버퍼 리더에 추가한다

    dfs
    1. 1 부터 9까지 실행하는 반복문을 만든다
    2. 스택의 크기가 n이 아닐경우 다시 dfs를 실행하게 만든다
    3. 만약 스택의 크기가 n 과 같을 경우 소수 인지 확인 한다.
    4. 만약 소수 일 경우 버퍼 리더에 추가한다

    isPrime

    1. stack에 있는 스트링을 다 더해 숫자로 만들고
    2. 소수 인지 확인 한다
    3. 소수 일 경우 버퍼 리더에 저장을 한다

 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class boj_2023 {

    static int N;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2023.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        N = Integer.parseInt(br.readLine());

        DFS(2, 1);
        DFS(3, 1);
        DFS(5, 1);
        DFS(7, 1);
    }

    static void DFS(int a, int b) {

        if (b == N) {
            if (isPrime(a)) {
                System.out.println(a);
            }
            return;
        }
        for (int i = 1; i <= 9; i++) {
            if (i % 2 == 0) {
                continue;
            }
            if (isPrime(a * 10 + i)) {
                DFS(a * 10 + i, b + 1);
            }
        }
    }

    static boolean isPrime(int num) {
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
