package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
    민호는 주어진 문자열로 암호를 만들어야된다. 하지만 (A, C, G, T)의 최소개수가 주어진다.
    최대 몇 개의 문자열을 만들수 있는 가???

    1. 문자열의 길이와 부분 문자열의 길이를 저장한다.
    2. DNA 문자열을 저장한다
    3. A, C, G, T 가 최소로 들어 가야되는 배열을 저장한다.
    4. myArr에 내가 가지고 있는 알파벳이 있을 경우 추가를 한다.
    5.

 */

public class boj_12891 {
    static int[] checkArr;
    static int[] myArr;
    static int checkSecret;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/12891.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int arrLength = Integer.parseInt(st.nextToken());
        int partArrLength = Integer.parseInt(st.nextToken());
        int Result = 0;
        checkSecret = 0;

        char[] arr = br.readLine().toCharArray();

        st = new StringTokenizer(br.readLine());

        for (int i = 0; i < 4; i++) {
            checkArr[i] = Integer.parseInt(st.nextToken());
            if (checkArr[i] == 0) {
                checkSecret++;
            }
        }
        for (int i = 0; i < partArrLength; i++) {
            Add(arr[i]);
        }
        if (checkSecret == 4) Result++;

        for (int i = partArrLength; i < arrLength; i++) {
            int j = i - partArrLength;
            Add(arr[i]);
            Remove(arr[j]);
            if (checkSecret == 4) Result++;
        }

        System.out.println(Result);
    }

    private static void Add(char c) {
        switch (c) {
            case 'A':
                myArr[0]++;
                if (myArr[0] == checkArr[0])
                    checkSecret++;
                break;
            case 'C':
                myArr[1]++;
                if (myArr[1] == checkArr[1])
                    checkSecret++;
                break;
            case 'G':
                myArr[2]++;
                if (myArr[2] == checkArr[2])
                    checkSecret++;
                break;
            case 'T':
                myArr[3]++;
                if (myArr[3] == checkArr[3])
                    checkSecret++;
                break;
        }
    }

    private static void Remove(char c) {
        switch (c) {
            case 'A':
                myArr[0]--;
                if (myArr[0] == checkArr[0])
                    checkSecret--;
                break;
            case 'C':
                myArr[1]--;
                if (myArr[1] == checkArr[1])
                    checkSecret--;
                break;
            case 'G':
                myArr[2]--;
                if (myArr[2] == checkArr[2])
                    checkSecret--;
                break;
            case 'T':
                myArr[3]--;
                if (myArr[3] == checkArr[3])
                    checkSecret--;
                break;
        }
    }
}
