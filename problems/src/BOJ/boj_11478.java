package BOJ;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

/*
   PACKAGE_NAME: boj_11478
   USER: choi
   DATE-TIME: 2023/06/23_9:46 AM
   
   1. 5 4 3 2 1
   2.
   3.
   4.
   5.
*/
public class boj_11478 {
	static HashSet<String> hashSet;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String ts = br.readLine();

		hashSet = new HashSet<String>();

		String name = "";

		for (int i = 0; i < ts.length(); i++) {
			name = "";

			for (int j = i; j < ts.length(); j++) {
				name += ts.substring(j, j + 1);
				hashSet.add(name);
			}
		}
		System.out.println(hashSet.size());
	}
}
