package BOJ;

/*
       경래 한테 총 n개 의 달걀이 있고 고객은 총 m 명이다. 각각의 i 번째 고객은 각자 달걀 하나를 p i 가격 이하로 살수 있다
       최대의 수익을 얻을려면 얼마에 팔고 합계는 얼마인가???

       1. n 과 m 값을 받아 저장한다
       2. 그 후 p 배열을 m 만큼 받고
       3. p 배열을 정렬 해준다
       4. for 문을 돌린다
       5. 만약 m - i 가 n 보다 적을경우 result 값을 p[i] * (m-1)을 넣어주고
       아닐경우에는 p[i] * n을 해준다
       6. 만약 result 가 현재 max 값 보다 클경우 price 를 p[i]을 넣어주고
       7. max에는 result 값을 넣어준다.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class boj_1246 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1246.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int N = Integer.parseInt(st.nextToken());
        int M = Integer.parseInt(st.nextToken());
        int coin = 0, max = 0;
        int[] arr = new int[M];

        for (int i = 0; i < M; i++) {
            arr[i] = Integer.parseInt(br.readLine());
        }

        Arrays.sort(arr);

        for (int i = 0; i < M; i++) {
            int result;
            if (M - i < N) {
                result = arr[i] * (M - i);
            } else {
                result = arr[i] * N;
            }

            if (max < result) {
                coin = arr[i];
                max = result;
            }
        }
        System.out.println(coin + " " + max);
    }
}
