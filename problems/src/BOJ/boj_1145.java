package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/*
    다섯개 수 중 적어도 3 개로 나누어 지는 배수를 찾아라

    1. 하나의 배열에 다섯 개의 수를 받는다
    2.
 */

public class boj_1145 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1145.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        int[] arr = new int[5];

        int min = 100;

        int cnt = 0;

        for (int i = 0; i < 5; i++) {
            arr[i] = Integer.parseInt(st.nextToken());
            if (min > arr[i]) {
                min = arr[i];
            }
        }

        Arrays.sort(arr);

        while (true) {
            for (int j : arr) {
                if (min % j == 0) cnt++;
            }

            if (cnt >= 3) break;

            cnt = 0;
            min++;
        }

        System.out.println(min);
    }
}
