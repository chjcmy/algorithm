package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * 첫째줄에 파일 이름의 개수 N이 주어진다. N은 50보다 작거나 같은 자연수이고 파일 이름의 길이는 모두 같고 길이는 최대 50이다
 * 파일이름은 알파벳 소문자와 . 로만 이루어져 있다
 * <p>
 * 1. N을 받아 변수에 저장한다
 * 2. N 만큼 문자를 받는다.
 * 3. result 문자열 배열을 추가한다
 * 4. 배열을 하나씩 비교하면서 다른 문자가 있을 경우 문자 위치에 ? 을 추가한다
 * 5. 마지막에 result 문자열을 출력한다
 */
public class boj_1032 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/1032.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        char[][] arr = new char[T][];

        StringTokenizer st;

        for (int i = 0; i < T; i++) {
            arr[i] = br.readLine().toCharArray();
        }

        if (T == 1) {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < arr[0].length; i++) {
                result.append(arr[0][i]);
            }
            System.out.println(result);
            return;
        }

        StringBuilder result = new StringBuilder();


        for (int i = 0; i < arr[0].length; i++) {
            char readChar = arr[0][i];
            for (char[] chars : arr) {
                if (readChar != chars[i]) {
                    readChar = '?';
                    break;
                }
            }
            result.append(readChar);
        }
        System.out.println(result);
        br.close();
    }
}
