package BOJ;

/*
    친구 인지 확인 해라
    어레이 리스트를 이용하여 dfs 를 이용하는 문제라고 생각한다
    푸는 법 처음 친구수를 정하고 그 친구수가 다음 친구 한테  나올 경유 true 로 빠져 나온다

    친구는 다섯명이다
  다 1. 사람수와 친구 관계 수를 변수에 저장한다
    2. 사람 수만큼 리스트 배열을 만든다
    3. 다음으로 관계 수만큼 처음 들어 온 사람은 본인이고 다음 변수는 친구로 arr[본인] 에 친구를 추가한다
    4. 친구의 크기만큼 반복문을 돌리면서 친구가 관계가 형성 되는지 확인한다
    5. 만약 친구 관계가 맞을 시 true로 끝나게 만든다
    6. 만약 다 끝나고도 false 일경우에는 0을 출력한다
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class boj_13023 {

    static ArrayList<Integer>[] friendArr;

    static boolean realFriend;

    static boolean[] visited;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/13023.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int N = Integer.parseInt(st.nextToken());
        int M = Integer.parseInt(st.nextToken());

        friendArr = new ArrayList[N];
        visited = new boolean[N];
        realFriend = false;

        for (int i = 0; i < N; i++) {
            friendArr[i] = new ArrayList<Integer>();
        }

        for (int i = 0; i < M; i++) {
            st = new StringTokenizer(br.readLine());

            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());

            friendArr[a].add(b);
            friendArr[b].add(a);
        }
        for (int i = 0; i < N; i++) {
            DFS(i, 1);
            if (realFriend) {
                break;
            }
        }
        if (realFriend) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }
    }

    static void DFS(int a, int b) {

        if (b == 5 || realFriend) {
            realFriend = true;
            return;
        }
        visited[a] = true;
        for (int c : friendArr[a]) {
            if (!visited[c]) {
                DFS(c, b + 1);
            }
        }
        visited[a] = false;
    }
}
