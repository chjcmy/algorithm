package BOJ;

/*
        임의의 수열을 스택에 넣었다가 출력하는 방식으로 오름차순 수열을 출력할 수 있는지 확인하고, 출력할 수 있다면 push와
        pop 연산을 어떤 순서로 수행해야 하는지를 알아내는 프로그램을 작성해야된다

        1. n을 받는다.
        2. n 만큼의 배열을 만든다
        3. n 만큼 수를 채운다
        4. stack을 만든다
        5. 반복문을 통해서 스택에 값을 채우고 값을 빼준다
        6. 만약 수열값이 오름차순 자연수: pop()을 수행해 수열 원소를 꺼낸다
        7. 스택의 가장 위의 수가 만들어야 하는 수열의 수보다 크면 수열을 출력할 수 없다
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class boj_1874 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1874.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(br.readLine());

        int[] arr = new int[n];

        Stack<Integer> stack = new Stack<>();

        StringBuffer bf = new StringBuffer();

        boolean result = true;

        for (int i = 0; i < n; i++) {
            arr[i] = Integer.parseInt(br.readLine());
        }

        int num = 1;

        for (int i = 0; i < arr.length; i++) {
            int su = arr[i];
            if (su >= num) {
                while (su >= num) {
                    stack.push(num++);
                    bf.append("+\n");
                }
                stack.pop();
                bf.append("-\n");
            } else {
                int nu = stack.pop();
                if (nu > su) {
                    System.out.println("NO");
                    result = false;
                    break;
                } else {
                    bf.append("-\n");
                }
            }
        }
        if (result) System.out.println(bf);
    }
}
