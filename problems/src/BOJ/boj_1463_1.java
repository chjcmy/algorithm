package BOJ;

/*
    1로 만들기
    1. 정수 x 를 받는다
    2. x+1 만큼 배열을 만든다
    3. x 만큼 3 과 2 으로 나누면 1 이 될때 까지 계산해준다
    4. 그후 배열의 x 의 위치 값을 표시해준다

 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class boj_1463_1 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1463.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int x = Integer.parseInt(br.readLine());

        int[] array = new int[x + 1];

        for (int i = 2; i <= x; i++) {
            array[i] = array[i - 1] + 1;

            if (i % 2 == 0) array[i] = Math.min(array[i], array[i / 2] + 1);
            if (i % 3 == 0) array[i] = Math.min(array[i], array[i / 3] + 1);
        }
        System.out.println(array[x]);
    }
}
