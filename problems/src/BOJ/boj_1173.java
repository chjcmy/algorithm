package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
    일분동안 운동을 하면 X + T 가 된다. M을 넘는것을 원하지 않기 때문에, X + T가 M 보다 작거나 같을 때만 운동할수있다.
    휴식을 선택하는 경우 맥박이 R만큼 감소한다. 영식이의 맥박이 X 였다면, 1분 동안 휴식을 한 후 맥박은 X-R이 된다.

    N 운동시간
    m 초기 맥박
    M 최대 맥박
    T 맥박 증가
    R 휴식

    1. N, m, M, T, R을 순서대로 받는다.
    2. pulse 라는 변수와 minute 라는 변수를 추가한다.
    3. 만약 minute 가 N 일 경우 반복 문을 끝낸다.
    4. 최대 맥박 보다 현재 맥박이 아래 일경우 운동 시간을 추가 하고 사용시간에 시간을 추가하고
        현재 맥박에 운동 맥박을 더 해준다.
    5. 최대 맥박 보다 현재 맥박이 많을 시 쉬고 시간을 추가한다.
    6. 운동 시간이 N 과 같을 시에 전체 적인 시간을 출력한다
 */

public class boj_1173 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1173.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        for (int i = 0; i < T; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int N = Integer.parseInt(st.nextToken());
            int m = Integer.parseInt(st.nextToken());
            int M = Integer.parseInt(st.nextToken());
            int t = Integer.parseInt(st.nextToken());
            int R = Integer.parseInt(st.nextToken());
            int init_m = m;
            int examTime = 0;
            int totalTime = 0;

            while (examTime != N) {
                totalTime++;

                if (m + t <= M) {
                    m += t;
                    examTime++;
                } else {
                    m -= R;
                    if (m < init_m) m = init_m;
                }
                if ((m + t > M) && (m == init_m))
                    break;
            }
            if (examTime != N) {
                System.out.println("-1");
            } else System.out.println(totalTime);
        }
    }
}
