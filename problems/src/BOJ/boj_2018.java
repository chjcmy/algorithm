package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/*
    1 부터 T 값 중에 연속적인 합이 T인걸 구하여라

    1. T 값을 받는다
    2. 1 부터 15 까지 반복 시킨다
    3. 카운트 출력 하기 위해 카운트 라는 변수를 추가 한다.
    4. 1 부터 15 까지 반복 시킬수 있는 반복문을 만든다
    5. 안에 sum 이라는 변수를 추가 하고 sum 이 만약 T와 같을 경우 cnt 에 1을 추가 하고 끝낸다
    6. 적을 경우에는 연속 적으로 수를 더하라
 */

public class boj_2018 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2018.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());
        int cnt = 1;
        int start_index = 1;
        int end_index = 1;
        int sum = 1;

        while (end_index != T) {
            if (sum == T) {
                cnt++;
                end_index++;
                sum = sum + end_index;
            } else if (sum > T) {
                sum = sum - start_index;
                start_index++;
            } else {
                end_index++;
                sum += end_index;
            }
        }
        System.out.println(cnt);
    }
}
