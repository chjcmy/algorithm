package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/*
    갑옷을 M 값에 맞춰 몇개 맞출수 있는지 알아 내라

    0. 시작 인덱스와 끝 인덱스, 합을 추가한다.
    1. 재료의 개수를 받는다
    2. 재료의 개수만큼 배열을 만든다
    3. 갑옷이 완성 되는 번호의 합을 저장한다
    4. 배열의 개수만큼 재료를 채워준다
    5. 배열을 sort 한다.
    6. 시작 인덱스가 재료의 개수가 될 때 멈추게 반복문을 만든다
    7. sum 은 시작 인덱스를 처음 부터 시작 하게 된다 .
    8. 만약 sum 이 M이 될경우 end_index에 1을 더해주고 end_index를 더해준다
    9. 만약 sum 이 M이 넘을경우 start_index를 빼준뒤에 start_index에 1 을 더해준다
    10. 만약 sum 이 M을 못 넘었을 경우 sum에 end_index를 더해주고 end_index에 1을 더해준다
    11. 끝이 나면 cnt를 출력해준다.
 */

public class boj_1940 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1940.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        int[] arr = new int[T];

        int M = Integer.parseInt(br.readLine());

        StringTokenizer st = new StringTokenizer(br.readLine());

        for (int i = 0; i < T; i++) {
            arr[i] = Integer.parseInt(st.nextToken());
        }

        Arrays.sort(arr);

        int cnt = 0;
        int start_index = 0;
        int end_index = T - 1;

        while (start_index < end_index) {
            if (arr[start_index] + arr[end_index] < M) {
                start_index++;
            } else if (arr[start_index] + arr[end_index] > M) {
                end_index--;
            } else {
                cnt++;
                start_index++;
                end_index--;
            }
        }
        System.out.println(cnt);
        br.close();
    }
}
