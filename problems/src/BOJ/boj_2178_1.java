package BOJ;

/*
    1. 전역 변수로 n과 m을 받는다
    2. 전역 변수로 미로 맵 과 미로를 탐색 했는지 참 거짓 배열을 만든다.
    3. 전역 변수로 미로 탐색할 동서남북을 만든다
    4. dfs 라는 함수를 만들고 큐를 만든다
*/


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class boj_2178_1 {
    static int N;
    static int M;
    static int[][] mazeMap;
    static boolean[][] mazeBoolean;
    static int[] moveX = {1, 0, -1, 0};
    static int[] moveY = {0, 1, 0, -1};

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2178.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        N = Integer.parseInt(st.nextToken());
        M = Integer.parseInt(st.nextToken());

        mazeMap = new int[N][M];
        mazeBoolean = new boolean[N][M];

        for (int i = 0; i < N; i++) {
            String string = br.readLine();
            for (int j = 0; j < M; j++) {
                mazeMap[i][j] = Integer.parseInt(string.substring(j, j + 1));
            }
        }

        BFS(0, 0);

        System.out.println(mazeMap[N - 1][M - 1]);
    }

    /*
    dfs
    1. 큐 배열을 만든다
    2. now 라는 변수를 넣고 현재 있는 위치를 저장한다
    3. 동서남북으로 4 번 돈다
    4. 현재 있는 위치 와 동서남북 배열을 더해서 plusX와 plusY 구한다
    5. 만약 plusX가 0 이상이고 M 을 넘지 않아야 할 경우와 plusY가 0 이상이고 M 을 넘지 않을 경우 다음 코드를 실행한다
    6. 그 이후 조건을 추가 하여 미로 맵의 위치가 plusX와 plusY 일때 0이 아니여야 하고 미로 참거짓 배열도 참이 아닐경우 다음 코드를 실행한다
    7. 미로맵에 plusX와 plusY에 미로 맵에 now 위치의 값에 1을 더하여 미로맵에 값을 변경 한다
    8. 미로 참 거짓 배열에
 */
    static void BFS(int x, int y) {
        Queue<int[]> queue = new LinkedList<>();
        queue.offer(new int[]{x, y});

        while (!queue.isEmpty()) {
            int[] now = queue.poll();
            mazeBoolean[x][y] = true;
            for (int i = 0; i < 4; i++) {
                int plusX = now[0] + moveX[i];
                int plusY = now[1] + moveY[i];

                if (plusX >= 0 && plusY >= 0 && plusX < N && plusY < M) {
                    if (mazeMap[plusX][plusY] != 0 && !mazeBoolean[plusX][plusY]) {
                        mazeBoolean[plusX][plusY] = true;
                        mazeMap[plusX][plusY] = mazeMap[now[0]][now[1]] + 1;
                        queue.add(new int[]{plusX, plusY});
                    }
                }
            }
        }

    }
}
