package BOJ;

/*
    디데이구하기, 단 윤년일 경우 윤년을 계산 하여 값을 출력하라

    * 4로 나눈어 떨어 질 경우 우선 윤년으로 한다
    * 단, 100으로 나누어 질 경우에는 평년이다.
    * 400으로 나누어떨어 질때는 다시 윤년이 된다.

    1. 처음 날과 마지막 날을 받는다
    &. 만약 처음 날의 년도와 마지막 날의 년도가 1001 년 차이가 난다면 gg를 치게 만든다
    2. 만약 처음 날과 마지막날 중간에 윤년이 있을 경우 +1 을 해준다
    3. sum 이라는 d-day 변수를 넣는다
    4. 처음 날짜 부터 sum 을 더 해준다.
    5. 1년이 지났을 경우 년도에 일년을 더 해준다
    6. 만약 마지막 년도와 마지막 달에 도달 했을 경우 마지막 일 수를 더 하고 끝낸다.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_1308 {

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1308.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int firstYear, firstMonth, firstDay;
        int secondYear, secondMonth, secondDay;


        StringTokenizer st = new StringTokenizer(br.readLine());
        firstYear = Integer.parseInt(st.nextToken());
        firstMonth = Integer.parseInt(st.nextToken());
        firstDay = Integer.parseInt(st.nextToken());
        st = new StringTokenizer(br.readLine());
        secondYear = Integer.parseInt(st.nextToken());
        secondMonth = Integer.parseInt(st.nextToken());
        secondDay = Integer.parseInt(st.nextToken());

        if (firstYear + 1000 < secondYear
                || (firstYear + 1000 == secondYear && firstMonth < secondMonth)
                || (firstYear + 1000 == secondYear && firstMonth == secondMonth && firstDay <= secondDay)
        ) {
            System.out.println("gg");
        } else {
            int dDay = 0;
            while (true) {
                firstDay++;
                dDay++;

                if (firstDay > 31 && (firstMonth == 1 || firstMonth == 3 || firstMonth == 5 || firstMonth == 7 ||
                        firstMonth == 8 || firstMonth == 10 || firstMonth == 12)) {
                    firstMonth++;
                    firstDay = 1;
                } else if (firstDay > 30 && (firstMonth == 4 || firstMonth == 6 || firstMonth == 9 ||
                        firstMonth == 11)) {
                    firstMonth++;
                    firstDay = 1;
                } else if (firstDay > 29 && firstMonth == 2 && check(firstYear)) {
                    firstMonth++;
                    firstDay = 1;
                } else if (firstDay > 28 && firstMonth == 2 && !check(firstYear)) {
                    firstMonth++;
                    firstDay = 1;
                }

                if (firstMonth > 12) {
                    firstYear++;
                    firstMonth = 1;
                }

                if (firstYear == secondYear && firstMonth == secondMonth && firstDay == secondDay) {
                    System.out.println("D-" + dDay);
                    break;
                }
            }
        }
    }

    public static boolean check(int year) { // 윤년
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }
}
