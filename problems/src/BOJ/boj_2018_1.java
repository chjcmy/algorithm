package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/*
    연속된 자연수의 합 구하기

    1. T 값을 받는다
    2. cnt 변수를 추가한다
    3. sum , start_index, end_index 를 추가한다.
    4. start_index 가 T값이 아닐 경우 계속 적으로 값을 구한다
    5. 만약 sum 이 T 일경우 end_index 값에 1을 추가하고 sum 에 end_index 값을 추가하고 cnt에 1을 추가한다.
    6. 만약 sum 이 T 값을 넘었을 경우에는 start_index를 sum에 빼주면서 start_index에 1 을 추가한다
    7. 만약 sum 이 T 값 보다 낮을 경우에는 end_index에 1을 추가 후에 end_index를 sum 에 추가한다
    8. 마지막으로 cnt 를 출력해준다
 */

public class boj_2018_1 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/2018.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        int cnt = 1;
        int sum = 1;
        int start_index = 1;
        int end_index = 1;

        while (start_index != T) {
            if (sum == T) {
                end_index++;
                sum += end_index;
                cnt++;
            } else if (sum > T) {
                sum -= start_index;
                start_index++;
            } else {
                end_index++;
                sum += end_index;
            }
        }
        System.out.println(cnt);
    }
}
