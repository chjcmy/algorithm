package BOJ;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class boj_1744 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int N = Integer.parseInt(br.readLine());

        int[] arrs = new int[N];
        boolean[] bool_arrs = new boolean[N];

        for (int i = 0; i < N; i++) {
            arrs[i] = Integer.parseInt(br.readLine());
        }

        Arrays.sort(arrs);

        int sum = 0;

        if (N == 1) { // 배열의 길이가 1인 경우
            System.out.println(arrs[0]); // arrs[0]을 출력하고 종료
            return;
        }

        for (int i = arrs.length - 1; i > 0; i--) {
            if (arrs[i] > 0 && arrs[i - 1] > 0) {
                bool_arrs[i - 1] = true;
                sum += arrs[i] * arrs[i - 1];
            } else if (arrs[i] == 0 && arrs[i - 1] <= 0) {
                bool_arrs[i - 1] = true;
                sum += arrs[i] * arrs[i - 1];
            } else if (arrs[i] > 0) {
                sum += arrs[i];
            }
        }

        if (!bool_arrs[0]) { // 배열의 첫 번째 원소가 남은 경우
            System.out.println(sum + arrs[0]); // 그냥 더해서 출력한다
        } else { // 배열의 모든 원소가 묶인 경우
            System.out.println(sum); // 결과값을 출력한다
        }
    }
}
