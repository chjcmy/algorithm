package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class boj_11660 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/11660.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int arrSize = Integer.parseInt(st.nextToken());
        int T = Integer.parseInt(st.nextToken());

        int[][] arr = new int[arrSize + 1][arrSize + 1];

        for (int i = 1; i < arr.length; i++) {

            st = new StringTokenizer(br.readLine());

            for (int j = 1; j < arr.length; j++) {
                arr[i][j] = Integer.parseInt(st.nextToken());
            }
        }

        int[][] plusArr = new int[arrSize + 1][arrSize + 1];

        for (int i = 1; i < arr.length; i++) {
            for (int j = 1; j < arr.length; j++) {
                plusArr[i][j] = plusArr[i][j - 1] + plusArr[i - 1][j] - plusArr[i - 1][j - 1] + arr[i][j];
            }
        }

        for (int i = 0; i < T; i++) {

            st = new StringTokenizer(br.readLine());

            int x1 = Integer.parseInt(st.nextToken());
            int y1 = Integer.parseInt(st.nextToken());
            int x2 = Integer.parseInt(st.nextToken());
            int y2 = Integer.parseInt(st.nextToken());

            System.out.println(plusArr[x2][y2] - plusArr[x1 - 1][y2] - plusArr[x2][y1 - 1] + plusArr[x1 - 1][y1 - 1]);
        }
    }
}
