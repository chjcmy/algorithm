package BOJ;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/*
    주어진 N개의 수에서 다른 두 수의 합으로 표현되는 수가 있다면 그 수를 '좋은 수'라고 한다.
    N개의 수 중 좋은 수가 총 몇 개인지 출력하시오

    1. N의 개수를 받는다
    2. N 만큼의 배열을 만든다.
    3. 1 부터 N 까지 투포인트 알고리즘을 이용하여 값이 있을시 브레이크 한다
    4. cnt 값을 출력 해준다
 */


public class boj_1253 {
    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/boj_text/1253.txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int T = Integer.parseInt(br.readLine());

        int[] arr = new int[T];

        int cnt = 0;

        StringTokenizer st = new StringTokenizer(br.readLine());

        for (int i = 0; i < T; i++) {
            arr[i] = Integer.parseInt(st.nextToken());
        }

        Arrays.sort(arr);

        for (int i = 0; i < T; i++) {
            int find = arr[i];
            int start_index = 0;
            int end_index = T - 1;

            while (start_index < end_index) {
                if (arr[start_index] + arr[end_index] == find) {
                    if (start_index != i && end_index != i) {
                        cnt++;
                        break;
                    } else if (start_index == i) {
                        start_index++;
                    } else {
                        end_index--;
                    }
                } else if (arr[start_index] + arr[end_index] < find) {
                    start_index++;
                } else {
                    end_index--;
                }
            }
        }
        System.out.println(cnt);
    }
}
