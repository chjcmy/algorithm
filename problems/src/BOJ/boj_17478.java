package BOJ;


class Parent {
    public void show() {
        System.out.println("Parent");
    }
}

class Child extends Parent {
    public void show() {
        super.show();
    }
}

public class boj_17478 {
    public static void main(String[] args) {
        Parent pa = new Child();

        pa.show();
    }
}
