package SWEXPORT;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class SW_14555 {

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/sw_text/sample_input (9).txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int test_case = Integer.parseInt(br.readLine());

        for (int i = 1; i <= test_case; i++) {

            int result = 0;

            String s_result = br.readLine();

            for (int j = 0; j < s_result.length() - 1; j++) {
                if (s_result.charAt(j) == '.') {
                    continue;
                }
                if (s_result.charAt(j) == '(' && s_result.charAt(j + 1) == '|' || s_result.charAt(j + 1) == ')') {
                    result++;
                }
            }

            System.out.printf("#%d %d\n", i, result);
        }
    }
}
