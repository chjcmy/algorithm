package SWEXPORT;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class sw_14692 {

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/sw_text/sample_input (8).txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int test_case = Integer.parseInt(br.readLine());

        for (int i = 1; i <= test_case; i++) {
            int result_count = Integer.parseInt(br.readLine());

            if (result_count % 2 == 0) {
                System.out.printf("#%d Alice\n", i);
            } else {
                System.out.printf("#%d Bob\n", i);
            }
        }
    }
}
