package SWEXPORT;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class sw_14413 {

    static char[][] board;
    static int[] arr;
    static int n, m = 0;

    private static void solution() {
        arr = new int[4];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (board[i][j] == '.') {
                    if ((i + j) % 2 == 0) arr[0]++;
                    else arr[1]++;
                } else if (board[i][j] == '#') {
                    if ((i + j) % 2 == 0) arr[2]++;
                    else arr[3]++;
                }
            }
        }

    }

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream("./src/sw_text/sample_input (10).txt"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int test_case = Integer.parseInt(br.readLine());

        for (int i = 1; i <= test_case; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());

            n = Integer.parseInt(st.nextToken());
            m = Integer.parseInt(st.nextToken());
            board = new char[n][m];

            for (int j = 0; j < n; j++) {
                st = new StringTokenizer(br.readLine());
                char[] st_arr = st.nextToken().toCharArray();
                System.arraycopy(st_arr, 0, board[j], 0, m);
            }

            solution();
            if ((arr[0] > 0 && arr[1] > 0) || (arr[2] > 0 && arr[3] > 0) || (arr[0] > 0 && arr[2] > 0) || (arr[1] > 0 && arr[3] > 0)) {
                System.out.println("#" + i + " impossible");
            } else {
                System.out.println("#" + i + " possible");
            }
        }
    }
}
