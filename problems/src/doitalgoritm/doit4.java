package doitalgoritm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class doit4 {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		StringTokenizer st;

		st = new StringTokenizer(br.readLine());

		int N = Integer.parseInt(st.nextToken());

		int M = Integer.parseInt(st.nextToken());

		int[][] arrs = new int[N + 1][N + 1];

		for (int i = 1; i < arrs.length; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 1; j < arrs[i].length; j++) {
				arrs[i][j] = Integer.parseInt(st.nextToken());
			}
		}

		for (int i = 0; i < M; i++) {
			st = new StringTokenizer(br.readLine());

			int sum = 0;

			int x1 = Integer.parseInt(st.nextToken());
			int y1 = Integer.parseInt(st.nextToken());

			int x2 = Integer.parseInt(st.nextToken());
			int y2 = Integer.parseInt(st.nextToken());

			sum = arrs[y2][x2] - arrs[y2][x1 - 1] - arrs[y1 - 1][x2] + arrs[y1 - 1][x1 - 1];

			StringBuilder sb = new StringBuilder();
			sb.append(sum);
			System.out.println(sb);
		}

	}

}