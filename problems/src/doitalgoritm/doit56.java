package doitalgoritm;

/*
   PACKAGE_NAME: 최단 경로 구하기
   USER: choi
   DATE-TIME: 7/16/23_3:13 PM

   노드 개수 : V , 에지의 개수 : E, 출발 노드 : K, 에지 배열: (u, v, w)

   결과 : 노드 개수 만큼 반복 하여 최단 거리 경로를 찾는다

   1. V, E 를 받는다
   2. K를 받는다
   3. V만큼의 인접 리스트를 만든다
   3. u, v, w를 받는다
    3.1 u의 배열에 v 인수에 w 값을 넣는다
    3.2 v의 배열에 u 인수에 w 값을 넣는다
   4. 노드 개수 만큼 반복 시킨다
    4.1 find 배열을 만든다
    4.2 해쉬맵 i 부터 수가 있는지 찾는다
    4.3
*/
public class doit56 {
}
