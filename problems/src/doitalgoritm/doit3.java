package doitalgoritm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class doit3 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		StringTokenizer st;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int dataCount = Integer.parseInt(br.readLine());

		int[] arrs = new int[dataCount + 1];

		st = new StringTokenizer(br.readLine());

		for (int i = 1; i < arrs.length; i++) {
			arrs[i] = Integer.parseInt(st.nextToken());
		}

		int testCount = Integer.parseInt(br.readLine());

		for (int i = 0; i < testCount; i++) {
			st = new StringTokenizer(br.readLine());

			int start = Integer.parseInt(st.nextToken());
			int end = Integer.parseInt(st.nextToken());

			int result = 0;

			for (int j = start; j <= end; j++) {
				result += arrs[j];
			}

			bw.write(result + "\n");
		}
		bw.flush();
		bw.close();
	}

}
