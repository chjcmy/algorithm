package doitalgoritm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * 연속 된 수중 n이 되는 수를 찾아라
 *
 * n 변수 저장
 * 사용 변수 초기화 ( count = 1, start_index = 1, end_index = 1, sum = 1)
 * while(end_index != N) {
 * 	if(sum == N) count 증가, end_index 증가, sum 값 변경
 *  else if(sum > N) sum값 변경, start_index 증가
 * 	else if(sum < N) end_index 증가, sum 값 변경
 * 	}
 * count 출력하
 * */

public class doit6 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int N = Integer.parseInt(br.readLine());

		int count = 0, start_index = 1, end_index = 1, sum = 1;

		while (end_index != N) {
			if (sum == N) {
				count++;
				end_index++;
				sum++;
			} else if (sum > N) {
				sum = sum - start_index;
				start_index++;
			} else {
				end_index++;
				sum = sum + end_index;
			}
		}

		System.out.println(count);
	}

}