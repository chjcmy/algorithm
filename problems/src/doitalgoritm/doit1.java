package doitalgoritm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
   PACKAGE_NAME: 숫자의 합 구하기
   USER: choi
   DATE-TIME: 2023/06/26_10:04 AM
   
   1.
   2.
   3.
   4.
   5.
*/
public class doit1 {
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int tc = Integer.parseInt(br.readLine());

		int result = 0;

		String a = br.readLine();

		for (int i = 0; i < a.length(); i++) {
			result += a.charAt(i) - '0';
		}

		System.out.println(result);
	}

}
