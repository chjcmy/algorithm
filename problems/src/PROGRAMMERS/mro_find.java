package PROGRAMMERS;

/*
   PACKAGE_NAME: miro_find
   USER: choi
   DATE-TIME: 2023/06/23_1:19 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class mro_find {
	/**
	 * 미로 탈출
	 * 격자 바깥으로 못나감
	 * n, m  미로 크기
	 * x, y 출발 위치
	 * r, c 탈출 위치
	 * k 이동 가능 거리
	 * 이동 가능 거리에 도착지점에 도착하면 리턴을 한다
	 **/

	static String answer;

	static int[][] miro;

	public static void main(String[] args) {
		solution(2, 2, 2, 3, 3, 1, 5);
	}

	public static String solution(int n, int m, int x, int y, int r, int c, int k) {
		answer = "";

		// 미로를 만든다
		miro = new int[m][n];

		// 반복 횟수를 만든다.
		int count = 0;
		// 길 찾기 함수를 만든다
		while (count < 4) {
			if (count == 0) {
				if (y + 1 < miro[y].length) {
					answer += "d";
					miro_find(x, y + 1, r, c, 1, k);
				}
			} else if (count == 1) {
				if (x - 1 < miro[y].length) {
					answer += "l";
					miro_find(x - 1, y, 1, r, c, k);
				}
			} else if (count == 2) {
				if (y - 1 >= 0) {
					answer += "u";
					miro_find(x, y - 1, 1, r, c, k);
				}
			} else if (count == 3) {
				if (x + 1 >= 0) {
					answer += "r";
					miro_find(x + 1, y, 1, r, c, k);
				}
			}
			if (answer.length() == k + 1) {
				break;
			}

		}

		if (answer.length() == 0) {
			answer = "impossible";
		}
		return answer;
	}

	public static void miro_find(int a, int b, int r, int c, int up, int tc) {


	}
}

