package java11;

import java.io.*;

/*
   PACKAGE_NAME: 골드바흐의 추측
   USER: choi
   DATE-TIME: 2023/03/04_3:45 PM
   
   1. 최대 소수를 만들수 있는 수를 받는다
   2. 수만큼 배열을 만든다
   3. 소수를 구한다
   4. 만약 제일 작은수부터 배열에 true 이면 더해보고 그 수가 입력된 수가 된다면 만약 x - y 를 했을경우 차이가 더 적을 경우 x y 에 저장한다
   5.
*/
public class bj_9020 {
	static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	public static void main(String[] args) throws IOException {


		int test_count = Integer.parseInt(br.readLine());

		for (int i = 0; i < test_count; i++) {
			int start_num = Integer.parseInt(br.readLine());

			goldbachPartition(start_num);
		}
		bw.flush();
		bw.close();
	}

	private static void goldbachPartition(int n) throws IOException {
		int a = n / 2;
		int b = n / 2;

		while (true) {
			if (isPrime(a) && isPrime(b)) {
				bw.write(a + " " + b + "\n");
				return;
			}
			a--;
			b++;
		}
	}

	private static boolean isPrime(int n) {
		if (n == 1) {
			return false;
		}
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
