package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 색종이
   USER: choi
   DATE-TIME: 2023/02/28_2:56 PM
   
   1. 사각형 갯수를 받는다
   2. 사각형의 x축 시작, 끝을 저장 할 수 있는 곳과 y축 시작, 끝을 저장한다
   3. 가로 가장 작은 수와 제일 큰수를 저장하고, 세로의 가장 작은 수와 제일 큰수를 저장한다
   4. 가로 제일 큰수에서 작은수를 뺀후 세로 가장 큰수에서 가장 작은 수를 뺀뒤 boolean 이차원 배열을 만든다.
   5. for 문을 이용하여 사각형 크기를 구해서 이차원 boolean 배열의 값을 true 로 바꾼다
   6. 바뀔시 count에 ++ 을 한다.
   7. count를 출력한다
*/
public class bj_2563 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int count_test = Integer.parseInt(br.readLine());

		boolean[][] booleans_square = new boolean[101][101];

		int count = 0;

		for (int i = 0; i < count_test; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());

			int x = Integer.parseInt(st.nextToken());
			int y = Integer.parseInt(st.nextToken());

			for (int j = x; j < x + 10; j++) {
				for (int k = y; k < y + 10; k++) {
					if (!booleans_square[j][k]) {

						booleans_square[j][k] = true;

						count++;
					}
				}
			}
		}

		bw.write(count + "");
		bw.flush();
		bw.close();
	}
}
