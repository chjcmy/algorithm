package java11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 단어의 개수
   USER: choi
   DATE-TIME: 2023/02/03_12:12 PM
   
   1. 문자를 받아 저장 한다
   2. string 배열을 만든다. string 배열에는 받은 문자를 스페이스로 나눈 게 들어간다.
   3. string 배열의 개수를 출력한다
   4.
   5.
*/
public class bj_1152 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		StringTokenizer st = new StringTokenizer(br.readLine(), " ");
		System.out.print(st.countTokens());
	}
}
