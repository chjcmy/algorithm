package java11;

import java.io.*;
import java.util.StringTokenizer;

public class bj_10871 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int count_int = Integer.parseInt(st.nextToken());

        int select_num = Integer.parseInt(st.nextToken());

        st = new StringTokenizer(br.readLine());

        for (int i = 0; i < count_int; i++) {
            int a = Integer.parseInt(st.nextToken());
            if (a < select_num) {
                bw.write(a + " ");
            }
        }
        bw.flush();
        bw.close();
    }
}
