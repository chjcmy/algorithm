package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 소수 찾기
   USER: choi
   DATE-TIME: 2023/03/02_3:02 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class bj_1978 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int test_count = Integer.parseInt(br.readLine());

		StringTokenizer st = new StringTokenizer(br.readLine());

		int result_count = 0;
		// 숫자를 받아서
		for (int i = 0; i < test_count; i++) {

			int test_num = Integer.parseInt(st.nextToken());

			if (isPrime(test_num)) {
				result_count++;
			}
		}
		bw.write(result_count + "");
		bw.flush();
		bw.close();
	}

	private static boolean isPrime(int num) {
		if (num == 1) {
			return false;
		}

		for (int i = 2; i <= Math.sqrt(num); i++) {
			if (num % i == 0) {
				return false;
			}
		}

		return true;
	}
}
