package java11;

import java.io.*;
import java.util.StringTokenizer;

public class bj_10807 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int result_count = 0;
        int count_int = Integer.parseInt(br.readLine());
        int[] int_arr = new int[count_int];

        StringTokenizer st = new StringTokenizer(br.readLine());
        for (int i = 0; i < int_arr.length; i++) {
            int_arr[i] = Integer.parseInt(st.nextToken());
        }

        int test_int = Integer.parseInt(br.readLine());

        for (int i = 0; i < int_arr.length; i++) {
            if (test_int == int_arr[i]) {
                result_count++;
            }
        }

        bw.write(result_count + "");
        bw.flush();
        bw.close();
    }
}
