package java11;

import java.io.*;

/*
   PACKAGE_NAME: lcs 최장 공통 부분 수열 만들기
   USER: choi
   DATE-TIME: 7/16/23_4:42 PM
   
   1. first과 Second 스트링 으로 저장
   2. 문자열 길이 저장
   3. 문자열 길이 + 1 캐릭터 배열 두개 생성
   4. 두개의 문자열을 1부터 각 문자를 캐릭터배열에 저장
   5. 정수형 이차원배열 dp 로 생성
   6. SecondCharArr 길이 만큼 반복한다
    6.1 firstCharArr 길이 만큼 반복 하게 만든다
        6.1.1 만약 firstCharrArr[i] 와 SecondCharArr[j]가 같다면
        6.1.2 dp[i-1][j-1] 에 1을 더 해 dp[i][j]에 더한다
        6.1.3 아닐시에 dp[i-1][j],dp[i][j -1]중 하나를 dp[i][j]에 대입한다
   7. 결과 값을 출력한다


*/
public class bj_9251 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		String A = br.readLine();
		String B = br.readLine();

		int aSize = A.length() + 1;
		int bSize = B.length() + 1;

		char[] aChar = new char[aSize];
		char[] bChar = new char[bSize];

		for (int i = 1; i < aChar.length; i++) {
			aChar[i] = A.charAt(i - 1);
		}
		for (int i = 1; i < bChar.length; i++) {
			bChar[i] = B.charAt(i - 1);
		}

		int[][] dp = new int[aSize][bSize];

		for (int i = 1; i < aSize; i++) {
			for (int j = 1; j < bSize; j++) {
				if (aChar[i] == bChar[j]) {
					dp[i][j] = dp[i - 1][j - 1] + 1;
				} else {
					dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
				}
			}
		}
		System.out.println(dp[aSize - 1][bSize - 1]);
	}
}
