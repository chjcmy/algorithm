package java11;

import java.io.*;
import java.util.Locale;

/*
   PACKAGE_NAME: 단어 공부
   USER: choi
   DATE-TIME: 2023/02/02_11:59 AM
*/
public class bj_1157 {
	static char[] alphabets = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		String test = br.readLine().toUpperCase(Locale.ROOT);

		int[] strings_check = new int[alphabets.length];

		int max_num = 0;

		int max_count = 0;

		for (int i = 0; i < test.length(); i++) {
			for (int j = 0; j < alphabets.length; j++) {
				if (alphabets[j] == test.charAt(i)) {
					strings_check[j]++;
				}
			}
		}

		for (int i = 0; i < strings_check.length; i++) {
			if (strings_check[max_num] < strings_check[i]) {
				max_num = i;
				max_count = 1;
			} else if (strings_check[max_num] == strings_check[i]) {
				max_count++;
			}
		}

		if (max_count > 1) {
			bw.write("?");
		} else {
			bw.write(alphabets[max_num] + "");
		}

		bw.flush();
		bw.close();
	}
}
