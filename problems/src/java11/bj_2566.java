package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 최댓값
   USER: choi
   DATE-TIME: 2023/02/27_12:04 AM
   
   1. first_max, second_max, max_num를 만든다
   2. 이단 중첩 문을 받는다
   3. max 보다 클경우 first_max 는 i, second_max 는 j를 넣는다
   4. max 를 출력하고 first_max 와 second_max를 출력한다
   5.
*/
public class bj_2566 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int first_max = 0, second_max = 0, max_num = 0;

		StringTokenizer st;

		for (int i = 1; i < 10; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 1; j < 10; j++) {
				int a = Integer.parseInt(st.nextToken());
				if (max_num <= a) {
					max_num = a;
					first_max = i;
					second_max = j;
				}
			}
		}
		bw.write(max_num + "\n" + first_max + " " + second_max);
		bw.flush();
		bw.close();
	}
}
