package java11;

import java.util.Scanner;

public class bj_2739 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        double result = sc.nextInt();

        double total = 0;

        double tc = sc.nextInt();

        for (int i = 0; i < tc; i++) {
            total += sc.nextInt() * sc.nextInt();
        }

        if (result == total) System.out.println("Yes");
        else System.out.println("No");
    }
}
