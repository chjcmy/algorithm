package java11;

import java.io.*;

/*
   PACKAGE_NAME: 소수
   USER: choi
   DATE-TIME: 2023/03/03_3:58 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class bj_2581 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int M = Integer.parseInt(br.readLine());
		int N = Integer.parseInt(br.readLine());

		int sum_result = 0;
		int small_int = 0;

		for (int i = M; i <= N; i++) {
			if (isPrime(i)) {
				sum_result += i;
				if (small_int == 0) {
					small_int = i;
				}
			}

		}

		if (sum_result == 0) {
			bw.write(-1 + "");
		} else {
			bw.write(sum_result + "\n" + small_int);
		}
		bw.flush();
		bw.close();
	}

	private static boolean isPrime(int n) {
		if (n == 1) {
			return false;
		}
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
