package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 상수
   USER: choi
   DATE-TIME: 2023/02/04_6:17 PM
   
   1. 문자 두개를 받는다.
   2. 반복문을 이용해 수를 거꾸로 바꾼다
   3. 둘을 비교 하여 더 큰수를 출력 한다
   4.
   5.
*/
public class bj_2908 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		String a = new StringBuilder(st.nextToken()).reverse().toString();
		String b = new StringBuilder(st.nextToken()).reverse().toString();

		if (Integer.parseInt(a) < Integer.parseInt(b)) {
			bw.write(b);
		} else {
			bw.write(a);
		}
		bw.flush();
		bw.close();
	}
}
