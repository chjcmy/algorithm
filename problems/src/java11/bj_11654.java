package java11;

import java.io.*;

/*
   PACKAGE_NAME: 아스키 코드
   USER: choi
   DATE-TIME: 2023/02/02_12:58 AM
*/
public class bj_11654 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		char a = br.readLine().charAt(0);

		int result = a;

		bw.write(result + "");
		bw.flush();
		bw.close();
	}
}
