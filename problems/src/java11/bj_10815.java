package java11;

import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 숫자 카드
   USER: choi
   DATE-TIME: 2023/03/21_1:01 AM
   
   1.
   2.
   3.
   4.
   5.
*/
public class bj_10815 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st;


		int first_arr_num = Integer.parseInt(br.readLine());
		int[] first_arr = new int[first_arr_num];
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < first_arr_num; i++) {
			first_arr[i] = Integer.parseInt(st.nextToken());
		}
		Arrays.sort(first_arr);
		int second_arr_num = Integer.parseInt(br.readLine());
		st = new StringTokenizer(br.readLine());
		for (int i = 0; i < second_arr_num; i++) {
			int target = Integer.parseInt(st.nextToken());
			bw.write((Arrays.binarySearch(first_arr, target) >= 0 ? 1 : 0) + "");
			bw.write(" ");
		}
		bw.flush();
		bw.close();
	}
}
