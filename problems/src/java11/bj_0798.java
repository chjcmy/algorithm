package java11;

import java.io.*;

/*
   PACKAGE_NAME: 세로 읽기
   USER: choi
   DATE-TIME: 2023/02/27_12:42 AM
   
   1. 케릭터 이차 행렬을 만든다
   2. 한줄씩 읽으면서 행렬을 집어 넣는다
   3. 첫 번째 열을 읽으면서 반복 한다
   4.
   5.
*/
public class bj_0798 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		char[][] chars_arr = new char[5][15];

		for (int i = 0; i < chars_arr.length; i++) {
			char[] target_char = br.readLine().toCharArray();

			System.arraycopy(target_char, 0, chars_arr[i], 0, target_char.length);
		}
		for (int k = 0; k < 1; k++) {
			for (int i = 0; i < chars_arr[k].length; i++) {
				for (int j = 0; j < 5; j++) {
					if (chars_arr[j][i] != 0) {
						bw.write(chars_arr[j][i] + "");
					}
				}
			}
		}

		bw.flush();
		bw.close();
	}
}
