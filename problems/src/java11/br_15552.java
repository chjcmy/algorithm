package java11;

import java.io.*;

public class br_15552 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        int tc = Integer.parseInt(br.readLine());
        StringBuilder stars = new StringBuilder();
        for (int i = 1; i <= tc; i++) {
            for (int k = 0; k < tc - i; k++) {
                stars.append(" ");
            }
            for (int j = 0; j < i; j++) {
                stars.append("*");
            }
            stars.append("\n");
        }
        bw.write(String.valueOf(stars));
        bw.flush();
        bw.close();
    }
}
