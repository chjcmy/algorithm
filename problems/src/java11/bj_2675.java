package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 문자열 반복
   USER: choi
   DATE-TIME: 2023/02/02_11:49 AM
*/
public class bj_2675 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int count = Integer.parseInt(br.readLine());

		StringTokenizer st;

		for (int i = 0; i < count; i++) {
			st = new StringTokenizer(br.readLine());

			int text_count = Integer.parseInt(st.nextToken());

			char[] a = st.nextToken().toCharArray();

			String b = "";

			for (int j = 0; j < a.length; j++) {
				for (int k = 0; k < text_count; k++) {
					b += a[j];
				}
			}

			bw.write(b + "\n");
		}
		bw.flush();
		bw.close();
	}
}
