package java11;

import java.io.*;
import java.util.StringTokenizer;

public class bj_10952 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        int[] arr = new int[2];

        StringTokenizer st;

        for (int i = 0; i < 5; i++) {

            st = new StringTokenizer(br.readLine());

            arr[0] = Integer.parseInt(st.nextToken());
            arr[1] = Integer.parseInt(st.nextToken());

            if (arr[0] == 0 & arr[1] == 0) {
                break;
            }

            bw.write((arr[0] + arr[1]) + "\n");
        }
        bw.flush();
        bw.close();

    }
}
