package java11;

import java.io.*;

public class bj_5597 {

    static boolean[] bool_arr;

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        bool_arr = new boolean[31];

        int count = 0;

        for (int i = 1; i <= 28; i++) {
            bool_arr[Integer.parseInt(br.readLine())] = true;
        }

        for (int i = 1; i < bool_arr.length; i++) {
            if (!bool_arr[i]) {
                bw.write(i + "\n");
                count++;
            } else if (count == 2) break;
        }

        bw.flush();
        bw.close();
    }

}
