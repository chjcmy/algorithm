package java11;

import java.io.*;

public class bj_1110 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int count = 1;

        int test_int = Integer.parseInt(br.readLine());

        int new_int = test_int;

        while (true) {
            int a = new_int / 10;
            int b = new_int % 10;
            new_int = ((a + b) % 10) + (b * 10);

            if (new_int != test_int) {
                count++;
            } else {
                break;
            }
        }

        bw.write(count + "");
        bw.flush();
        bw.close();
    }
}
