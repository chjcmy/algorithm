package java11;

import java.io.*;

public class bj_15596 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test_count = Integer.parseInt(br.readLine());

        int[] ints = new int[test_count];

        for (int i = 0; i < test_count; i++) {
            ints[i] = Integer.parseInt(br.readLine());
        }

        long result_int = sum(ints);

        bw.write(result_int + "");
        bw.flush();
        bw.close();

    }

    private static long sum(int[] a) {

        long result = 0;

        for (int i = 0; i < a.length; i++) {
            result += a[i];
        }

        return result;
    }
}
