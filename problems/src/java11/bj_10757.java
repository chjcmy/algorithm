package java11;

import java.io.*;
import java.math.BigInteger;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 큰수 A + B
   USER: choi
   DATE-TIME: 2023/03/20_11:40 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class bj_10757 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		BigInteger A = new BigInteger(st.nextToken());

		BigInteger B = new BigInteger(st.nextToken());

		BigInteger result = A.add(B);

		bw.write(result + "");
		bw.flush();
		bw.close();
	}
}
