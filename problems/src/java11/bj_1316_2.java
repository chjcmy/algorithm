package java11;

import java.io.*;

/*
   PACKAGE_NAME: 그룹 단어 체커 복습
   USER: choi
   DATE-TIME: 2023/02/23_3:32 PM
   
   1. 테스트 수를 받는다 처음 테스트 카운트를 0으로 초기화 한다
   2. 테스트 수 만큼 반복 한다.
   3. 단어를 받는다
   3.1 알파벳 개수 만큼 boolean 배열을 만들어 준다
   4. 단어의 길이 만큼 반복 한다
   5. 단어를 캐릭터로 바꾼뒤에 'a' 를 빼주고 정수를 받는다
   6. 만약 단어 순서가 0이 보다 크고 알파벳이 false 거나 전 순서 알파벳이 같다면 그룹 단어를 false로 만들고 끝낸다
   7. 해당 알파벳 출현 체크를 한다
   8. 만약 그룹단어일 경우 카운터를 추가한다
   9. 카운터를 마지막에 출력해준다
*/
public class bj_1316_2 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int test_count = Integer.parseInt(br.readLine());

		int count = 0;

		for (int i = 0; i < test_count; i++) {
			String test_string = br.readLine();
			boolean[] alphabet_arr = new boolean[26];
			boolean isGroupWord = true;

			for (int j = 0; j < test_string.length(); j++) {
				int char_int = test_string.charAt(j) - 'a';

				if (j > 0 && char_int != test_string.charAt(j - 1) - 'a' && alphabet_arr[char_int]) {
					isGroupWord = false;
					break;
				}

				alphabet_arr[char_int] = true;
			}

			if (isGroupWord) {
				count++;
			}
		}

		bw.write(count + "");
		bw.flush();
		bw.close();
	}
}
