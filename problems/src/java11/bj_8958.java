package java11;

import java.io.*;

public class bj_8958 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int count = Integer.parseInt(br.readLine());

        for (int i = 0; i < count; i++) {

            char[] a = br.readLine().toCharArray();

            int string_count = 0;

            int result = 0;

            for (int j = 0; j < a.length; j++) {
                if (a[j] == 'O') {
                    string_count++;
                    result += string_count;
                } else {
                    string_count = 0;
                }
            }
            bw.write(result + "\n");
        }
        bw.flush();
        bw.close();
    }
}
