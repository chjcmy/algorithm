package java11;

import java.io.*;

/*
   PACKAGE_NAME: 다이얼
   USER: choi
   DATE-TIME: 2023/02/06_4:15 PM
   
   1. 문자를 캐릭터로 받는다.
   2. 문자에서 캐릭터로 나눈뒤에 저장을 한다
   3. 숫자를 비교하여 result에 저장한다
   4.
   5.
*/
public class bj_5622 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		char[] chars = br.readLine().toCharArray();

		int result = 0;

		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == 'A' || chars[i] == 'B' || chars[i] == 'C') {
				result += 3;
			} else if (chars[i] == 'D' || chars[i] == 'E' || chars[i] == 'F') {
				result += 4;
			} else if (chars[i] == 'G' || chars[i] == 'H' || chars[i] == 'I') {
				result += 5;
			} else if (chars[i] == 'J' || chars[i] == 'K' || chars[i] == 'L') {
				result += 6;
			} else if (chars[i] == 'M' || chars[i] == 'N' || chars[i] == 'O') {
				result += 7;
			} else if (chars[i] == 'P' || chars[i] == 'Q' || chars[i] == 'R' || chars[i] == 'S') {
				result += 8;
			} else if (chars[i] == 'T' || chars[i] == 'U' || chars[i] == 'V') {
				result += 9;
			} else {
				result += 10;
			}
		}

		bw.write(result + "");
		bw.flush();
		bw.close();
	}
}
