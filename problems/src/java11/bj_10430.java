package java11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class bj_10430 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int[] int_arr = new int[3];

        int result = 0;

        int a = Integer.parseInt(br.readLine());

        int b = Integer.parseInt(br.readLine());

        for (int i = 2; i >= 0; i--) {
            int_arr[i] = (int) (b / Math.pow(10, i));
            b %= Math.pow(10, i);
        }

        for (int i = 0; i < 3; i++) {
            System.out.println(a * int_arr[i]);
            result += (int) (a * int_arr[i] * Math.pow(10, i));
        }

        System.out.println(result);
    }
}
