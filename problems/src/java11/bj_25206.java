package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 너의 평점은
   USER: choi
   DATE-TIME: 2023/02/24_2:47 AM
   20줄 과목을 받으면서 switch 문으로 과목점수를 비교한다(단, 과목은 계산에서 제외)
   1. 크기가 20인 배열을 받는다
   2. 만약 p가 아닐경우 스위치 문으로 연속적으로 받는 과목 점수를 비교한다 count에 학점을 추가 한다
   3. 마지막에 총 점수을 학점 크기로 나눈고 출력한다
   4.
   5.
*/
public class bj_25206 {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		double count_sum = 0;
		double full_count_major = 0;

		StringTokenizer st;

		for (int i = 0; i < 20; i++) {

			st = new StringTokenizer(br.readLine());

			st.nextToken();

			double count_major = Double.parseDouble(st.nextToken());

			String major_string_score = st.nextToken();

			if (!major_string_score.equals("P")) {
				full_count_major += calcGrade(count_major, major_string_score);
				count_sum += count_major;
			}
		}

		if (full_count_major != 0) {
			full_count_major /= count_sum;
		}
		
		bw.write(full_count_major + "");
		bw.flush();
		bw.close();
	}

	private static double calcGrade(double score, String grade) {
		double sum = 0;

		switch (grade) {
			case "A+":
				sum = score * 4.5;
				break;

			case "A0":
				sum = score * 4.0;
				break;

			case "B+":
				sum = score * 3.5;
				break;

			case "B0":
				sum = score * 3.0;
				break;

			case "C+":
				sum = score * 2.5;
				break;

			case "C0":
				sum = score * 2.0;
				break;

			case "D+":
				sum = score * 1.5;
				break;

			case "D0":
				sum = score;
				break;
		}
		return sum;
	}
}
