package java11;

import java.io.*;

/*
   PACKAGE_NAME: 숫자의 합
   USER: choi
   DATE-TIME: 2023/02/02_1:10 AM
*/
public class bj_11720 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		String count = br.readLine();
		String a = br.readLine();

		int result = 0;

		for (int i = 0; i < a.length(); i++) {
			result += Character.getNumericValue(a.charAt(i));
		}

		bw.write(result + "");
		bw.flush();
		bw.close();
	}
}

