package java11;

import java.io.*;

public class bj_2562 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int[] int_arr = new int[10];

        int max_select = 0;

        for (int i = 1; i < int_arr.length; i++) {
            int_arr[i] = Integer.parseInt(br.readLine());
        }

        for (int i = 1; i < int_arr.length; i++) {

            if (int_arr[i] > int_arr[max_select]) max_select = i;

        }

        bw.write(int_arr[max_select] + "\n" + max_select);
        bw.flush();
        bw.close();
    }
}
