package java11;

import java.io.*;

/*
   PACKAGE_NAME: 크로아티아 알파벳
   USER: choi
   DATE-TIME: 2023/02/08_12:25 PM
   
   1.
   2. 알파벳 배열과 크로아티아 배열을 비교한다.
   3. 만약 맞는게 있고 지금 위치의 + 1 이 알파벳 스트링 배열 보다 작다면 다음 스트링과 비교한다.
   4. 만약 그게 맞다면 result에 + 1 을 한고 i에 + 1을 한다
   5. result를 마지막에 출력한다

*/
public class bj_2941 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int result = 0;

		String a = br.readLine();

		for (int i = 0; i < a.length(); i++) {
			if (a.charAt(i) == 'c' && i < a.length() - 1) {
				if (a.charAt(i + 1) == '=' || a.charAt(i + 1) == '-') { // c= c-
					i++;
				}
			} else if (a.charAt(i) == 'd' && i < a.length() - 1) {
				if (a.charAt(i + 1) == 'z') {
					if (i < a.length() - 2 && a.charAt(i + 2) == '=') { // dz=
						i += 2;
					}
				} else if (a.charAt(i + 1) == '-') { // d-
					i++;
				}
			} else if (a.charAt(i) == 'l' || a.charAt(i) == 'n') {
				if (i < a.length() - 1 && a.charAt(i + 1) == 'j') { // lj nj
					i++;
				}
			} else if (a.charAt(i) == 's' || a.charAt(i) == 'z') {
				if (i < a.length() - 1 && a.charAt(i + 1) == '=') { // s= z=
					i++;
				}
			}
			result++;
		}
		bw.write(result + "");
		bw.flush();
		bw.close();
	}
}