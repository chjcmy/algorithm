package java11;

import java.io.*;

public class bj_3052 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        boolean[] booleans_arr = new boolean[42];

        int count = 0;

        for (int i = 1; i <= 10; i++) {
            booleans_arr[(Integer.parseInt(br.readLine()) % 42)] = true;
        }

        for (int i = 0; i < 42; i++) {
            if (booleans_arr[i]) {
                count++;
            }
        }

        bw.write(count + "");
        bw.flush();
        bw.close();
    }
}
