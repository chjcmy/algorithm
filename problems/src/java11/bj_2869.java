package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 달팽이는 올라가고 싶다
   USER: choi
   DATE-TIME: 2023/03/20_5:08 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class bj_2869 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int A = Integer.parseInt(st.nextToken());
		int B = Integer.parseInt(st.nextToken());
		int V = Integer.parseInt(st.nextToken());

		int day = (V - B) / (A - B);
		if ((V - B) % (A - B) != 0) {
			day++;
		}

		bw.write(day + "");
		bw.flush();
		bw.close();
	}
}
