package java11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/*
   PACKAGE_NAME: 알파벳 찾기
   USER: choi
   DATE-TIME: 2023/02/02_11:26 AM
*/
public class bj_10809 {
	static char[] alphabets = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		char[] a = br.readLine().toCharArray();
		int[] ints = new int[alphabets.length];
		int count = 0;

		Arrays.fill(ints, -1);

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < alphabets.length; j++) {
				if (a[i] == alphabets[j]) {
					if (ints[j] == -1) {
						ints[j] = count;
					}
					count++;
				}
			}
		}

		for (int i = 0; i < ints.length; i++) {
			System.out.println(ints[i]);
		}
	}
}
