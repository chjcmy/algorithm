package java11;

import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class bj_1546 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int count = Integer.parseInt(br.readLine());

        double[] result = new double[count];

        StringTokenizer st = new StringTokenizer(br.readLine());

        double summation = 0;

        for (int i = 0; i < count; i++) {

            result[i] = Double.parseDouble(st.nextToken());
        }

        Arrays.sort(result);

        for (int i = 0; i < result.length; i++) {
            summation += (result[i] / result[count - 1]) * 100;
        }

        bw.write((summation / count) + "");
        bw.flush();
        bw.close();
    }
}
