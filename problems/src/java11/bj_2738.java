package java11;

import java.io.*;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 행렬 덧셈
   USER: choi
   DATE-TIME: 2023/02/26_5:18 PM
   
   1. 숫자에 맞게 두개의 행렬을 만든다
   2. 처음 행렬에 두번 째 행렬을 더한다
   3. 첫번째 행렬을 출력한다
   4.
   5.
*/
public class bj_2738 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		StringTokenizer st = new StringTokenizer(br.readLine());

		int first_int = Integer.parseInt(st.nextToken());
		int second_int = Integer.parseInt(st.nextToken());

		int[][] first_arr = new int[first_int][second_int];

		for (int i = 0; i < first_int; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < second_int; j++) {
				first_arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		for (int i = 0; i < first_int; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < second_int; j++) {
				first_arr[i][j] += Integer.parseInt(st.nextToken());
			}
		}

		for (int i = 0; i < first_int; i++) {
			for (int j = 0; j < second_int; j++) {
				bw.write(first_arr[i][j] + " ");
			}
			bw.write("\n");
		}
		bw.flush();
		bw.close();
	}
}
