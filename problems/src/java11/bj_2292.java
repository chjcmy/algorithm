package java11;

import java.io.*;

/*
   PACKAGE_NAME: 벌집
   USER: choi
   DATE-TIME: 2023/03/01_3:42 PM
   363811849016
    1 7 19 37 61
     6 12 18 24
   1. 수를 받는다
   2. while 문을 사용한다
   3. 더하는 수는 6부터 시작하며 처음수는 1 부터 시작한다
   4. 더하는 수에 6을 더해주면서 처음 수에 반복 적으로 더해준다 그러면서 count++을 해준다
   5. 만약 처음수가 받은 수보다 크다면 끝낸다
*/
public class bj_2292 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int test_count = Integer.parseInt(br.readLine());

		int count = 1;

		int start_int = 1;

		int plus_int = 6;

		while (true) {
			if (test_count <= start_int) {
				break;
			}

			start_int += plus_int;
			count++;
			plus_int += 6;
		}
		
		bw.write(count + "");
		bw.flush();
		bw.close();
	}
}
