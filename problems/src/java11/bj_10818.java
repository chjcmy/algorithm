package java11;

import java.io.*;
import java.util.StringTokenizer;

public class bj_10818 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int max = 0, min = 0;

        int count = Integer.parseInt(br.readLine());

        StringTokenizer st = new StringTokenizer(br.readLine());

        for (int i = 0; i < count; i++) {

            int a = Integer.parseInt(st.nextToken());

            if (i == 0) {
                max = a;
                min = a;
            }

            if (a > max) max = a;
            if (a < min) min = a;
        }

        bw.write(min + " " + max);
        bw.flush();
        bw.close();
    }
}
