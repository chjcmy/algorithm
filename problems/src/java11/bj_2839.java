package java11;

import java.io.*;

/*
   PACKAGE_NAME: 설탕 배달
   USER: choi
   DATE-TIME: 2023/03/02_1:53 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class bj_2839 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		// 테스트 수를 받는다.
		int test_count = Integer.parseInt(br.readLine());

		// 나중에 출력할 값을 넣는다
		int result_count = 0;

		// 테스트 받는 수를 5로 나눈뒤에 result_count에 더한다
		// 테스트 카운트를 5 로 나눈 나머지를 test_count에 저장한다

		while (test_count > 0) {
			if (test_count % 5 == 0) {
				result_count += test_count / 5;
				test_count %= 5;
			} else {
				test_count -= 3;
				result_count++;
			}
		}

		if (test_count < 0) {
			bw.write("-1");
		} else {
			bw.write(result_count + "");
		}
		bw.flush();
		bw.close();
	}
}
