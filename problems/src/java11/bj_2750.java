package java11;

import java.io.*;
import java.util.Arrays;

/*
   PACKAGE_NAME: 수 정렬하기
   USER: choi
   DATE-TIME: 2023/03/05_4:58 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class bj_2750 {
	public static void main(String[] args) throws IOException {
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int test_num = Integer.parseInt(br.readLine());

		int[] test_arr = new int[test_num];

		for (int i = 0; i < test_num; i++) {
			test_arr[i] = Integer.parseInt(br.readLine());
		}

		Arrays.sort(test_arr);

		for (int i = 0; i < test_num; i++) {
			bw.write(test_arr[i] + "\n");
		}
		bw.flush();
		bw.close();
	}
}
