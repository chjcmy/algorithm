package java11;

/*
        1. 테스트 갯수를 받는다
        2. 테스트 갯수마다 반복문을 돌린다
        3. 평균을 구하고 저장을 한다
        4. 반복문을 돌려서 평균 이하를 갯수를 구한다
        5. 갯수 / 테스트 갯수 * 100 을 출력한다
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class bj_4344 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int test_count = Integer.parseInt(br.readLine());

        StringTokenizer st;

        for (int i = 0; i < test_count; i++) {
            st = new StringTokenizer(br.readLine());

            float count = Integer.parseInt(st.nextToken());

            int sum = 0;

            int avr_person = 0;

            int[] ints = new int[(int) count];

            for (int j = 0; j < count; j++) {
                ints[j] = Integer.parseInt(st.nextToken());
                sum += ints[j];
            }

            float avr = sum / count;

            for (int j = 0; j < count; j++) {
                if (ints[j] > avr) avr_person++;
            }

            double result = (avr_person * 100) / count;

            System.out.printf("%.3f%%\n", result);
        }
    }
}
