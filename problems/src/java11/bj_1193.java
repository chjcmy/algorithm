package java11;

import java.io.*;

/*
   PACKAGE_NAME: 분수 찾기
   USER: choi
   DATE-TIME: 2023/03/01_9:56 PM
   
   1. 위치를 받는다
   3. count 와 분자와 분모를 만든다
   4. 한번 왔다 갔을 때 마다 count 에 1을 추가 한다
   5. 홀 수 일경우 count 를 x 에 넣고 y를 1붙터 for문을 시작시킨다
   6. 짝수일경우 count를 y에 넣고 x를 1부터 for문을 시작 시킨다.
*/
public class bj_1193 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int test_count = Integer.parseInt(br.readLine());

		int cross_count = 1, prev_count_sum = 0;

		while (true) {
			if (test_count <= prev_count_sum + cross_count) {
				if (cross_count % 2 == 1) {
					bw.write((cross_count - (test_count - prev_count_sum - 1)) + "/"
							+ (test_count - prev_count_sum));
					break;
				} else {
					bw.write(((test_count - prev_count_sum)) + "/"
							+ (cross_count - (test_count - prev_count_sum - 1)));
					break;
				}
			} else {
				prev_count_sum += cross_count;
				cross_count++;
			}
		}
		bw.flush();
		bw.close();
	}
}
