package java11;

import java.io.*;

/*
   PACKAGE_NAME: 그룹 단어 체커
   USER: choi
   DATE-TIME: 2023/02/09_2:04 PM

   단어를 캐릭터 배열로 만든다. 반복문을 돌리면서 다른게 있다면 연속 갯수를 0으로 만들고


*/
public class bj_1316 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		int test_count = Integer.parseInt(br.readLine());
		int count = 0;

		for (int i = 0; i < test_count; i++) {
			String word = br.readLine();

			boolean[] check = new boolean[26];
			boolean isGroupWord = true;

			for (int j = 0; j < word.length(); j++) {
				int index = word.charAt(j) - 'a';

				if (j > 0 && index != word.charAt(j - 1) - 'a' && check[index]) {
					isGroupWord = false;
					break;
				}

				check[index] = true;
			}

			if (isGroupWord) {
				count++;
			}

		}

		bw.write(count + "");
		bw.flush();
		bw.close();
	}
}
