package text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Base64;

/*
   PACKAGE_NAME: 베이스 64 디코드
   USER: choi
   DATE-TIME: 2023/05/11_3:11 AM
   
   1.
   2.
   3.
   4.
   5.
*/
public class sw1928 {
	public static void main(String[] args) throws IOException {
		System.setIn(new FileInputStream("./src/sw_text/1928.txt"));

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int tc = Integer.parseInt(br.readLine());

		for (int i = 1; i <= tc; i++) {
			String a = br.readLine();

			byte[] decodedbytes = Base64.getUrlDecoder().decode(a);

			String result = new String(decodedbytes);

			System.out.printf("%s\n", result);
		}
	}
}
