package text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/*
   PACKAGE_NAME: 새로운 불면증 치료법
   USER: choi
   DATE-TIME: 2023/05/11_3:27 AM
   
   1.
   2.
   3.
   4.
   5.
*/
public class sw1288 {
	public static void main(String[] args) throws IOException {
		System.setIn(new FileInputStream("./src/sw_text/1288.txt"));

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int tc = Integer.parseInt(br.readLine());

		for (int i = 1; i <= tc; i++) {
			boolean[] booleans = new boolean[10];

			int N = Integer.parseInt(br.readLine());

			int result = 1;

			while (true) {

				int bool_int = 0;

				char[] chars = Integer.toString(N * result).toCharArray();

				for (int j = 0; j < chars.length; j++) {
					booleans[chars[j] - '0'] = true;
				}

				for (int j = 0; j < 10; j++) {
					if (booleans[j]) {
						bool_int++;
					}
				}

				if (bool_int == 10) {
					result *= N;
					break;
				}

				result++;
			}

			System.out.printf("#%d %d\n", i, result);

		}
	}
}
