package text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
   PACKAGE_NAME: 가랏! rc카
   USER: choi
   DATE-TIME: 2023/05/10_5:10 PM
   
   1.
   2.
   3.
   4.
   5.
*/
public class sw1940 {
	public static void main(String[] args) throws IOException {
		System.setIn(new FileInputStream("./src/sw_text/1940.txt"));

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int tc = Integer.parseInt(br.readLine());

		StringTokenizer st;

		for (int i = 1; i <= tc; i++) {
			int count = Integer.parseInt(br.readLine());
			int now_speed = 0, now_which = 0;

			for (int j = 0; j < count; j++) {
				st = new StringTokenizer(br.readLine());

				int get_call = Integer.parseInt(st.nextToken());

				int get_speed = 0;

				if (get_call != 0) {
					get_speed = Integer.parseInt(st.nextToken());
				}

				if (get_call == 1) {
					now_speed += get_speed;
					now_which += now_speed;
				} else if (get_call == 2) {
					now_speed -= get_speed;
					if (now_speed < 0) {
						now_speed = 0;
					}
					now_which += now_speed;
				} else {
					now_which += now_speed;
				}
			}

			System.out.printf("#%d %d\n", i, now_which);
		}
	}
}
